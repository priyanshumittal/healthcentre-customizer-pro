<?php/**
* @Theme Name	:	Health-Center
* @file         :	index-project.php
* @package      :	Health-Center
* @author       :	Hari Maliya
* @license      :	license.txt
* @filesource   :	wp-content/themes/health-center/index-project.php
*/
?>
<!-- HC Portfolio Section -->
<?php $current_options = wp_parse_args( get_option('hc_pro_options', array() ), theme_data_setup());
if($current_options['project_enable'] == false) { ?>
<div class="container">
	<?php if($current_options['portfolio_title'] || $current_options['portfolio_description'] ) { ?>
	<div class="row">
		<div class="section-header">
			<?php if($current_options['portfolio_title']!='') { ?>
			<h1 class="section-title"><?php echo $current_options['portfolio_title']; ?></h1>
			<?php } ?>
			<?php if($current_options['portfolio_description']!='') { ?>
			<p class="section-subtitle"><?php echo $current_options['portfolio_description']; ?></p>
			<?php } ?>			
		</div>
	</div>		
	<?php  } if(is_active_sidebar('sidebar-project'))
	{
		echo '<div id="sidebar-project" class="row sidebar-project">';
		dynamic_sidebar('sidebar-project');
		echo '</div>';
	}
	else
	{
		$current_options = get_option('hc_pro_options');
		
		echo '<div id="sidebar-project" class="row sidebar-project">';
		if(!empty($current_options)) {
			get_template_part('index','projecttwo');
		}	
		echo '</div>';
	}
	?>

	<div class="row"><div class="hc_home_border"></div></div>
</div><?php } ?>
<!-- /HC Portfolio Section -->