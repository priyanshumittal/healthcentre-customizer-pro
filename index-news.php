<!-- HC Recent News & Why Choose us Section -->
<div class="container">
	<?php $current_options = wp_parse_args( get_option('hc_pro_options', array() ), theme_data_setup()); ?>
	
	<div id="sidebar-news" class="row sidebar-news">
	
		<!--Recent News-->
		<div class="col-md-6">
		<?php
		if($current_options['news_enable'] == false){
			if(is_active_sidebar('sidebar-news'))
			{
				dynamic_sidebar('sidebar-news');
			}
			else
			{
				$current_options = get_option('hc_pro_options');
				if(!empty($current_options)) {
					get_template_part('index','news_two');
				}	
			}
		}
		?>
		</div>
		
		<!--Why choose us-->
		<div class="col-md-6 hc_accordion_section">
		<?php 
		$current_options = wp_parse_args( get_option('hc_pro_options', array() ), theme_data_setup());
		if($current_options['faq_enable']==false): ?>
		
			<?php if($current_options['hc_head_faq']!='') { ?>
			<div class="hc_heading_title">
				<h3><?php echo $current_options['hc_head_faq']; ?></h3>
			</div>
			<?php } ?>
			
			<div class="panel-group" id="accordion">			
				<?php 
				 if( is_active_sidebar('sidebar-faq') ){
					dynamic_sidebar('sidebar-faq');
				 }
				 else
				{
				$current_options = wp_parse_args( get_option('hc_pro_options', array() ), theme_data_setup());
				if(!empty($current_options)) {
				get_template_part('index','faqs');
				}	
				}
				?>
			</div>
		
		<?php endif; ?>
		</div>	
		
	</div>
</div>