<div class="row">
	<?php  
	$current_options = wp_parse_args( get_option('hc_pro_options', array() ), theme_data_setup());
	$j=1;
	$default_arg =array('class' => "index_ser_img" );	
    $total_services = $current_options['service_list']; 
		$args = array( 'post_type' => 'healthcenter_service','posts_per_page' =>$total_services); 	
	$service = new WP_Query( $args ); 
	if( $service->have_posts() )
	{ while ( $service->have_posts() ) : $service->the_post(); ?>	
	       <?php  $link=1;
		          if(get_post_meta( get_the_ID(),'meta_service_link', true )) 
					{ $meta_service_link=get_post_meta( get_the_ID(),'meta_service_link', true ); }
					else { $link=0 ; } ?>	
		<div class="col-md-3 hc_service_area">	
		     <?php 	if(has_post_thumbnail()){ ?>
                 <?php if ($link==1) { ?>
					<a href="<?php echo $meta_service_link; ?>" <?php  if(get_post_meta( get_the_ID(),'meta_service_target', true )) { echo "target='_blank'"; }  ?> > <?php the_post_thumbnail('',$default_arg); ?>
					</a> 
					<?php } else {  the_post_thumbnail('',$default_arg); } ?>
					  
			<?php } else { ?> 
			      <?php if( $link==1) { ?>
					<a href="<?php echo $meta_service_link; ?>" <?php  if(get_post_meta( get_the_ID(),'meta_service_target', true )) { echo "target='_blank'"; }  ?>><i class="fa <?php echo get_post_meta( get_the_ID(),'service_icon_image', true ) ; ?>"></i>	</a>
					<?php } else { ?> <i class="fa <?php echo get_post_meta( get_the_ID(),'service_icon_image', true ) ; ?>"></i>	
            <?php }  } if($link==1) {  ?>
            			
			<h2><a href="<?php echo $meta_service_link; ?>" <?php  if(get_post_meta( get_the_ID(),'meta_service_target', true )) { echo "target='_blank'"; }  ?> ><?php echo the_title(); ?></a></h2>
			<?php } else { ?> <h2><a> <?php echo the_title(); ?> </a></h2><?php } ?>
			
			
			<p><?php echo get_home_service_excerpt(); ?></p>
			   <?php if($link==1) { ?>
			<p><a href="<?php echo $meta_service_link; ?>"><?php _e('Read More','health'); ?> <span class="fa fa-arrow-circle-right hc_service_reamore_icon"></span></a></p>
			<?php } ?>
		</div>
	<?php if($j%4==0){	echo "<div class='clearfix'></div>"; } $j++;  endwhile; 
	} ?>
	</div>