Health-Center-Pro.

A Free Blue colored Business Blog theme that supports Primary menu's , Primary sidebar,Four widgets area at the footer region  etc. 
It has a perfect design that's great for any Business/Firms  Blogs who wants a new look for their site. Three page templates Home ,Blog and Contact Page. 
Health-Center-Pro supports featured slider managed from Theme Option Panel.

Author: Priyanshu Mittal,Hari Maliya,Shahid Mansuri and Vibhor Purandare.
Theme Homepage Url:http://webriti.com/demo/wp/healthcentre/

About:
Health-Center-Pro a theme for business, consultancy firms etc  by Priyanshu Mittal (Author URI: http://www.webriti.com). 

The CSS, XHTML and design is released under GPL:
http://www.opensource.org/licenses/gpl-license.php

Feel free to use as you please. I would be very pleased if you could keep the Auther-link in the footer. Thanks and enjoy.

Appoinment supports Custom Menu, Widgets and 
the following extra features:

 - Pre-installed menu and content colors
 - Responsive
 - Custom sidebars
 - Support for post thumbnails
 - Similar posts feature
 - 4 widgetized areas in the footer
 - Customise Front Page 
 - Custom footer
 - Translation Ready 
 

# Basic Setup of the Theme.
-----------------------------------
Fresh installation!

1. Upload the Health-Center-Pro Theme folder to your wp-content/themes folder.
2. Activate the theme from the WP Dashboard.
3. Done!
=== Images ===

All images in Health-Center-Pro are licensed under the terms of the GNU GPL.

# Top Navigation Menu:
- Default the page-links start from the left! Use the Menus function in Dashboard/Appearance to rearrange the buttons and build your own Custom-menu. DO NOT USE LONG PAGE NAMES, Maximum 14 letters/numbers incl. spaces!
- Read more here: http://codex.wordpress.org/WordPress_Menu_User_Guide

=============Page Templates======================
1. Contact  Page Tempalte:- Create a page as you do in WordPress and select the page template with the name 'Contact'


===========Front Page Added with the theme=================
1 It has header(logo + menus),Home Featured Image, services,recent comments widgets and footer.

======Site Title and Description=============
Site Title and its description in not shown on home page besides this both are used above each page / post along with the search field.
	
Support
-------

Do you enjoy this theme? Send your ideas - issues - on the theme formn . Thank you!
@version 1.7.5
1. Added Italian Locale.
@version 1.7.4
1. Add Wide layout.
2. Updated Turkish Locale.
3. Added Spanish & Russian Locale.
@version 1.7.3
1. Updated Turkish Locale.
@version 1.7.2
1. Changed widget name.
@version 1.7.1
1. Updated Strings.
@version 1.7
1. Added Turkish Locale
@version 1.6.9
1. Update Pot File.
2. Add Font awesome icon library 4.7.0
@version 1.6.8
1. Change opacity effect.
@version 1.6.7
1. Fixed Jetpack gallery overlay issue.
@version 1.6.6
1. Added Jetpack plugin support and Gallery overlay css.
@version 1.6.5
1. Support for excerpt in feature page widget service content.
@version 1.6.4
1. Update Service / Page widget for Post thumbnails.
@version 1.6.3
1. Remove sanitize funtion in team post and allow html in team widget.
@version 1.6.2
1. Remove whitespace in font.php file.
@version 1.6.1
1. Add a setting for renaming the Portfolio Taxonomy / Category slug name.
2. Solved number of styling issue's found during theme unit test data.
@version 1.6
1. Add Widget for Home-page section.
@version 1.5.5
1. Update Font Awesome Icon 4.5.0 to 4.6.3
2. Add Title Tag Support
3. Add two-columns, three-columns, four-columns, left-sidebar, right-sidebar, footer-widgets, blog, custom-background, custom-menu,translation-ready, portfolio
@version 1.5.4
1. typograpy settings issue resolved.
@version 1.5.3
1. Add Powered By Filed in Footer copyright section.
@version 1.5.2
1. Solved Home Project Issue
@version 1.5.1
1. Added wpml Support.
@version 1.5
1. Added Customizer Setting.
@version 1.4
1. Add Slider template.
2. Change font-awesome Librery 4.0.3 to 4.4.0
@version 1.3.1
Slider Issue Fixed.
@version 1.3
1. Add Skin color.
@version 1.2.1
1. Fixed clearfix issue in Service Template and Portfolio Template.
2. Fixed issue blank html generated on blank data.
3. add border in project title and summary.
@version 1.2
1.Add taxonomy Portfolio archive.
@version 1.1.3
1. Add Custom Background Feature.
@version 1.1.2
1.Portfolio default category issue fixed. 
@version 1.1.0.1
1.REPLY-TO ADDED IN CONTACT FORM. 
@version 1.1.0
1.Slider Synch With Carofredsal Slider
@version 1.1
1.Typography Bug Fixed.
@Version 1.0
released

# --- EOF --- #