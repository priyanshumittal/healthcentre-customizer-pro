<?php
/**Theme Name	: Health-Center
 * Theme Core Functions and Codes
*/	
	/**Includes reqired resources here**/
	define('WEBRITI_TEMPLATE_DIR_URI',get_template_directory_uri());	
	define('WEBRITI_TEMPLATE_DIR',get_template_directory());
	define('WEBRITI_THEME_FUNCTIONS_PATH',WEBRITI_TEMPLATE_DIR.'/functions');	
	
	require_once('theme_setup_data.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/menu/default_menu_walker.php' ); // for Default Menus
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/menu/webriti_nav_walker.php' ); // for Custom Menus	
	
	require_once( WEBRITI_THEME_FUNCTIONS_PATH . '/scripts/scripts.php' ); // all js and css file for health-center	
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/post-type/custom-post-type.php' );// for health-center cpt
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/meta-box/post-meta.php' );// for health-center meta box
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/taxonomies/taxonomies.php' );// for health-center taxonomies
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/resize_image/resize_image.php' ); //for image resize
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/excerpt/excerpt.php' ); // for Excerpt Length
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/commentbox/comment-function.php' ); //for comments
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/custom-sidebar.php' ); //for widget register
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/font/font.php'); //Google Font
	
	// Footer widgets
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/wbr-footer-contact-widgets.php' ); //for footer custom widgets
	// Sidebar Widgets
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/wbr-sidebar-latest-news.php' ); //for sidebar Latest News custom widgets

	//Faq widget
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/wbr-faq-widget.php' );	
	
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/pagination/webriti_pagination.php' ); //webriti pagination class
	
	//Feature Page Widget
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/wbr-register-page-widget.php' );
	
	//Project Widget
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/wbr-project-widget.php' );
	
	//Testimonail Widget
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/wbr-testimonial-widget.php' );
	
	//Team Widget
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/wbr-team-widget.php' );
	
	// Resent News Widget
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/wbr-resent-news-widget.php' );
	
	// header icon info widget
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/widget/wbr-icon-info-widget.php' );
	
	//Customizer
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_theme_color.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_header.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_portfolio_category_archive.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_typography.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_post_slug.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_template.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_copyright.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer_layout.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/slider-panel.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/service-panel.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/project-panel.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/testimonials-panel.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/faq-panel.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/recent_news-panel.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/callout-panel.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/additional-panel.php');
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/customizer/customizer-pro.php');
	
	
	
	require( WEBRITI_TEMPLATE_DIR . '/css/custom-color-skin.php');
	
	
	//Webriti shortcodes
	require( WEBRITI_THEME_FUNCTIONS_PATH . '/shortcodes/shortcodes.php' ); //for shortcodes 	
	
	//content width
	if ( ! isset( $content_width ) ) $content_width = 900;		
	//wp title tag starts here
	function webriti_head( $title, $sep )
	{	global $paged, $page;		
		if ( is_feed() )
			return $title;
		// Add the site name.
		$title .= get_bloginfo( 'name' );
		// Add the site description for the home/front page.
		$site_description = get_bloginfo( 'description' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			$title = "$title $sep $site_description";
		// Add a page number if necessary.
		if ( $paged >= 2 || $page >= 2 )
			$title = "$title $sep " . sprintf( _e( 'Page', 'health' ), max( $paged, $page ) );
		return $title;
	}	
	add_filter( 'wp_title', 'webriti_head', 10,2 );
	
	add_action( 'after_setup_theme', 'webriti_setup' ); 	
	function webriti_setup()
	{	// Load text domain for translation-ready
		load_theme_textdomain( 'health', WEBRITI_THEME_FUNCTIONS_PATH . '/lang' );
		
		function custom_excerpt_length( $length ) {	return 50; }
		add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
		function new_excerpt_more( $more ) {	return '';}
		add_filter('excerpt_more', 'new_excerpt_more');
		
		function cleanup_shortcode_fix($content) {  
              $array = array ('<p>[' => '[',']</p>' => ']',']<br />' => ']',']<br>' => ']','<p>  </p>'=>'');              
			  $content = strtr($content, $array);				
			return $content;
            }
		add_filter('the_content', 'cleanup_shortcode_fix'); 
		
		add_theme_support( 'post-thumbnails' ); //supports featured image
		// This theme uses wp_nav_menu() in one location.
		register_nav_menu( 'primary', __( 'Primary Menu', 'health' ) );
		// theme support 	
		$args = array('default-color' => '000000',);
		add_theme_support( 'custom-background', $args  ); 
		add_theme_support( 'automatic-feed-links');
		add_theme_support( 'title-tag');
		
			
		// setup admin pannel defual data for index page		
		$health_center_pro_theme=theme_data_setup();
		//add_option('hc_pro_options', $health_center_pro_theme);
		
		//print_r($health_center_pro_theme);
		$current_theme_options = get_option('hc_pro_options'); // get existing option data
		
		
		/**
		* Register Post format
		*/
		add_theme_support( 'post-formats', array( 'quote' ) );
		
		//Custom logo
		add_image_size('hc-logo', 150, 50);
		add_theme_support('custom-logo', array('size' => 'hc-logo'));
		
		// do shortcode 
		add_filter('widget_text', 'do_shortcode');
		
		
	}
	// Read more tag to formatting in blog page 
	function new_content_more($more)
	{  global $post;
	   return ' <a href="' . get_permalink() . "#more-{$post->ID}\" class=\"hc_blog_btn\">" .__('Read More','health')."<i class='fa fa-long-arrow-right'></i></a>";
	}   
	add_filter( 'the_content_more_link', 'new_content_more' );
	
	// custom background
function custom_background_function()
{
	$hc_pro_options=theme_data_setup(); 
    $current_options = wp_parse_args(  get_option( 'hc_pro_options', array() ), $hc_pro_options );
	$page_bg_image_url = $current_options['hc_back_image'];
	
	if($page_bg_image_url!='none.png')
	{
	echo '<style>body.health_center_background{ background-image:url("'.WEBRITI_TEMPLATE_DIR_URI.'/images/bg-patterns/'.$page_bg_image_url.'");}</style>';
	}
}
add_action('wp_head','custom_background_function',10,0);



add_filter( 'body_class', 'health_body_classes' );
/**
 * body classes
 *
 */
function health_body_classes( $classes ) {
		$hc_pro_options=theme_data_setup(); 
		$current_options = wp_parse_args(  get_option( 'hc_pro_options', array() ), $hc_pro_options );
		if($current_options['hc_back_image'] !='none.png') {
		$classes[] = 'health_center_background';
		}
	

	return $classes;
}

function custom_excerpt($new_length = 20, $new_more = '...' , $from = 'all' ) {
	
  add_filter('excerpt_length', function () use ($new_length) {
    return $new_length;
  }, 999);
  
  add_filter('excerpt_more', function () use ($new_more) {
    return $new_more;
  });
  
  $output = get_the_excerpt();
  $output = apply_filters('wptexturize', $output);
  $output = apply_filters('convert_chars', $output);
  
  if($from=='testimonial'){
	 $output = '<p>'. $output .'</p>'; 
  }
  else if( $from=='slider' ){
	
	$output = strip_tags(preg_replace(" (\[.*?\])",'',$output));
	$output = strip_shortcodes($output);		
	$original_len = strlen($output);
	$output = substr($output, 0, 155);		
	$len=strlen($output);
	
	if($original_len>155) {
		$output = '<div class="slide-text-bg2">' .'<span>'.$output.'</span>'.'</div>'.
					   '<div class="slide-btn-area-sm"><a href="' . get_permalink() . '" class="slide-btn-sm">'.__("Read More","health").'</a></div>';
	}else{
		$output = '<div class="slide-text-bg2">' .'<span>'.$output.'</span>'.'</div>';
	}
	
  }
  
  echo $output;
}

add_action( 'init', 'add_excerpts_to_pages' );
function add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}
?>