<?php
/*	@Theme Name	:	Health-Center
* 	@file         :	fullwidth-page.php
* 	@package      :	Health-Center
* 	@author       :	VibhorPurandare
* 	@license      :	license.txt
* 	@filesource   :	wp-content/themes/health-center/fullwidth-page.php
*/
//Template Name:Full Width Page
?>
<?php get_header(); ?>
<!-- HC Page Header Section -->	
<div class="container">
	<div class="row">
		<div class="hc_page_header_area">
			<?php the_post(); ?>
			<h1><?php the_title(); ?></h1>			
		</div>
	</div>
</div>
<!-- /HC Page Header Section -->
<!-- HC Blog right Sidebar Section -->	
<div class="container">
	<div class="row hc_blog_wrapper">
		
		<!--Blog Content-->
		<div class="col-md-12">
				<div class="hc_blog_detail_section">
					<div class="hc_blog_post_img">
						<?php $defalt_arg =array('class' => "img-responsive" ); ?>
						<?php if(has_post_thumbnail()): ?>
						<a  href="<?php the_permalink(); ?>">
							<?php the_post_thumbnail('full', $defalt_arg); ?>
						</a>
						<?php endif; ?>				
					</div>
					<div class="hc_blog_post_content"><p><?php the_content( __( 'Read More' , 'health' ) ); ?></p></div>	
				</div>				
		</div>
		
	</div>
	</div>
<?php get_footer(); ?>