<?php	
		/**
		Template Name: Home Page
		*/
		$hc_pro_options=theme_data_setup();
		$current_options = wp_parse_args(  get_option( 'hc_pro_options', array() ), $hc_pro_options );
		get_header();
		get_template_part('index','slider');
		$data =is_array($current_options['front_page_data']) ? $current_options['front_page_data'] : explode(",",$current_options['front_page_data']);
		if($data) 
		{
			foreach($data as $key=>$value)
			{			
				switch($value) 
				{	
					case 'Service': 
					//****** get index service  ********
					get_template_part('index', 'service') ;
					break;
					
					case 'Project':
					//****** get index project  ********
					get_template_part('index', 'project');					
					break;
					
					case 'News': 			
					//****** get index recent blog  ********
					get_template_part('index', 'news');					
					break; 	
					
					case 'Testimonials': 			
					//****** get index recent blog  ********
					get_template_part('index', 'testimonials');					
					break;
					
					case 'Additional': 			
					//****** get index Additional Section  ********
					get_template_part('index', 'additional');					
					break;
					
					case 'CallOut': 			
					//****** get index recent blog  ********
					get_template_part('index', 'calloutarea');					
					break;  
				}
			}	
			
	get_footer(); 
	}
?>