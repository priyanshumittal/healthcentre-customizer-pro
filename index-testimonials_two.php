
<!-- Testimonial Section -->
<div class="container">
<?php $current_options = wp_parse_args( get_option('hc_pro_options', array() ), theme_data_setup()); ?>
	<div class="row">
		<div class="col-md-12">
			<div class="hc_heading_title">
			<?php if($current_options['hc_head_testimonial']!='') { ?>
				<h3><?php echo $current_options['hc_head_testimonial']; ?></h3>				
				<?php } ?>
				<div class="hc_carousel-navi">
					<a id="prev3" class="hc_carousel-prev" href="#mytestimonial" data-slide="prev"><i class="fa fa-angle-left"></i></a>
					<a id="next3" class="hc_carousel-next" href="#mytestimonial" data-slide="next"><i class="fa fa-angle-right"></i></a>		
				</div>
			</div>
		</div>
	</div>
	<div id="mytestimonial" class="carousel slide" data-ride="carousel" data-type="multi">	
		<div class="carousel-inner">
		<?php $count_posts = wp_count_posts( 'health_testimonial')->publish;			
			$args = array( 'post_type' => 'health_testimonial','posts_per_page' =>$count_posts) ; 	
			$testis = new WP_Query( $args );
			if( $testis->have_posts() )
			{	
				$i=1;
				while ( $testis->have_posts() ) : $testis->the_post();
				$meta_author_designation =  get_post_meta( get_the_ID(),'author_designation_meta_save', true ); 
				$meta_desc =  get_post_meta( get_the_ID(),'description_meta_save', true ); 
			?>
				<div class="item <?php if($i==1) { echo 'active';} $i++; ?>">
					<div class="col-md-6 col-sm-6 col-xs-12 hc_testimonials_area">
						<div class="hc_testimonials_area_content">
							<p><?php echo $meta_desc; ?></p>				
						</div>
						<div class="hc_testimonials_area_content_bottom_arrow ">
							<div class="inner_area"></div>
						</div>
						<div class="hc_testimonials_user">
							<div class="hc_testimonials_avatar_wrapper">
										<div class="hc_testimonials_avatar">
										<?php if( has_post_thumbnail() ): ?>
										<?php the_post_thumbnail( 'thumbnail', array( 'class' => 'img-responsive','style'=>'height:100%;' ) )?>
										<?php endif; ?>
										</div>
									</div>
							
							
							<h4 class="hc_testimonials_title">
								
								<?php the_title(); ?>
							
								<?php if( get_post_meta(get_the_ID(),'designation', true ) ): ?>
								<br/><small><?php echo get_post_meta( get_the_ID(),'designation', true); ?></small>
								<?php endif; ?>
								
							</h4>
							<div class="hc_testimonials_position"><?php echo $meta_author_designation; ?></div>
						</div>
					</div>
			   </div>	
				<?php endwhile;
			} ?>
			</div>
		</div>			
</div>
<!-- /Testimonial Section -->
<script>
		jQuery(function($) {

			//	Testimonial Scroll Js	
			
			$('#mytestimonial').carousel({
			  interval: 10000
			})

			$('#mytestimonial .item').each(function(){
					
			  var next = $(this).next();
			  if (!next.length) {
				next = $(this).siblings(':first');
			  }
			  next.children(':first-child').clone().appendTo($(this));
			  
			  for (var i=0;i<0;i++) {
				next=next.next();
				if (!next.length) {
					next = $(this).siblings(':first');
				}
				
				next.children(':first-child').clone().appendTo($(this));
			  }
			});
			
				
		});
</script>