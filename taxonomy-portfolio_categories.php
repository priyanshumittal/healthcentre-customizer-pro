<?php get_header();  

$terms = get_the_terms( $post->ID , 'portfolio_categories' );
foreach ( $terms as $term ) {
 
}
?>	
<!-- HC Page Header Section -->	
<?php $current_options = wp_parse_args( get_option('hc_pro_options', array() ), theme_data_setup()); ?>
<div class="container">
	<div class="row">
		<div class="hc_page_header_area">
			<h1><?php echo $term->name; ?></h1>	
		</div>
	</div>
</div>
<!-- /HC Page Header Section -->	
<!-- HC Portfolio 2 Column Section -->	
<div class="container">	
	<div class="tab-content hc_main_portfolio_section" id="myTabContent">
		<?php 						
			if(have_posts() )
			{ 	
			 $norecord=0; $j=1;
			?>
			
			<div class="row">
			<?php while (have_posts()) : the_post(); ?>
			<?php if($current_options['taxonomy_portfolio_list']==2) {

					if(get_post_meta( get_the_ID(),'meta_project_link', true )) 
					{ $meta_project_link=get_post_meta( get_the_ID(),'meta_project_link', true ); }
					else { $meta_project_link = get_post_permalink(); } ?>
					<div class="col-md-6 hc_portfolio_area">
						<div class="hc_portfolio_showcase">
							<div class="hc_portfolio_showcase_media">								
								<?php if(has_post_thumbnail())
								{ 
									$class=array('class'=>'hc_img_responsive');
									the_post_thumbnail('full', $class);
									$post_thumbnail_id = get_post_thumbnail_id();
									$post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id );
								} ?>	
								<div class="hc_portfolio_showcase_overlay">
									<div class="hc_portfolio_showcase_overlay_inner">
										<div class="hc_portfolio_showcase_icons">
											<a href="<?php echo $meta_project_link; ?>" <?php  if(get_post_meta( get_the_ID(),'meta_project_target', true )) { echo "target='_blank'"; }  ?> ><i class="fa fa-link"></i></a>
											<a class="hover_thumb" data-lightbox="image" href="<?php echo $post_thumbnail_url; ?>"><i class="fa fa-picture-o"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="hc_portfolio_caption">
							<h3><a href="<?php echo $meta_project_link; ?>" <?php  if(get_post_meta( get_the_ID(),'meta_project_target', true )) { echo "target='_blank'"; }  ?> ><?php the_title(); ?></a></h3>
							<small><?php echo get_post_meta( get_the_ID(),'portfolio_project_summary', true ); ?></small>		
						</div>
					</div>
					<?php } ?>
					<?php if($current_options['taxonomy_portfolio_list']==3) {

					if(get_post_meta( get_the_ID(),'meta_project_link', true )) 
					{ $meta_project_link=get_post_meta( get_the_ID(),'meta_project_link', true ); }
					else { $meta_project_link = get_post_permalink(); } ?>
					<div class="col-md-4 hc_portfolio_area">
						<div class="hc_portfolio_showcase">
							<div class="hc_portfolio_showcase_media">								
								<?php if(has_post_thumbnail())
								{ 
									$class=array('class'=>'hc_img_responsive');
									the_post_thumbnail('full', $class);
									$post_thumbnail_id = get_post_thumbnail_id();
									$post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id );
								} ?>	
								<div class="hc_portfolio_showcase_overlay">
									<div class="hc_portfolio_showcase_overlay_inner">
										<div class="hc_portfolio_showcase_icons">
											<a href="<?php echo $meta_project_link; ?>" <?php  if(get_post_meta( get_the_ID(),'meta_project_target', true )) { echo "target='_blank'"; }  ?> ><i class="fa fa-link"></i></a>
											<a class="hover_thumb" data-lightbox="image" href="<?php echo $post_thumbnail_url; ?>"><i class="fa fa-picture-o"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="hc_portfolio_caption">
							<h3><a href="<?php echo $meta_project_link; ?>" <?php  if(get_post_meta( get_the_ID(),'meta_project_target', true )) { echo "target='_blank'"; }  ?> ><?php the_title(); ?></a></h3>
							<small><?php echo get_post_meta( get_the_ID(),'portfolio_project_summary', true ); ?></small>		
						</div>
					</div>
					<?php } ?>
					<?php if($current_options['taxonomy_portfolio_list']==4) {

					if(get_post_meta( get_the_ID(),'meta_project_link', true )) 
					{ $meta_project_link=get_post_meta( get_the_ID(),'meta_project_link', true ); }
					else { $meta_project_link = get_post_permalink(); } ?>
					<div class="col-md-3 hc_portfolio_area">
						<div class="hc_portfolio_showcase">
							<div class="hc_portfolio_showcase_media">								
								<?php if(has_post_thumbnail())
								{ 
									$class=array('class'=>'hc_img_responsive');
									the_post_thumbnail('full', $class);
									$post_thumbnail_id = get_post_thumbnail_id();
									$post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id );
								} ?>	
								<div class="hc_portfolio_showcase_overlay">
									<div class="hc_portfolio_showcase_overlay_inner">
										<div class="hc_portfolio_showcase_icons">
											<a href="<?php echo $meta_project_link; ?>" <?php  if(get_post_meta( get_the_ID(),'meta_project_target', true )) { echo "target='_blank'"; }  ?> ><i class="fa fa-link"></i></a>
											<a class="hover_thumb" data-lightbox="image"  href="<?php echo $post_thumbnail_url; ?>"><i class="fa fa-picture-o"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="hc_portfolio_caption">
							<h3><a href="<?php echo $meta_project_link; ?>" <?php  if(get_post_meta( get_the_ID(),'meta_project_target', true )) { echo "target='_blank'"; }  ?> ><?php the_title(); ?></a></h3>
							<small><?php echo get_post_meta( get_the_ID(),'portfolio_project_summary', true ); ?></small>		
						</div>
					</div>
					<?php } ?>
					<?php if($j%$current_options['taxonomy_portfolio_list']==0){ echo "<div class='clearfix'></div>"; } $j++; $norecord=1; endwhile; ?>
				</div>
			<?php 
			}  wp_reset_query(); ?>
	
</div>	
</div>
<?php get_footer(); ?>