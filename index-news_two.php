<?php $current_options = wp_parse_args( get_option('hc_pro_options', array() ), theme_data_setup());?>
<div class="hc_post_section">	
		
		<?php if($current_options['hc_head_news']!='') { ?>
		<div class="hc_heading_title">
			<h3><?php echo $current_options['hc_head_news']; ?>	</h3>	
		</div>
		<?php } ?>	
		
		<?php 
		$args = array( 'post_type' => 'post','posts_per_page' =>3,'post__not_in'=>get_option("sticky_posts")); 	
		query_posts( $args );
		if(query_posts( $args ))
		{	while(have_posts()):the_post();
			{ 	
			?>
			<div class="media hc_post_area">			
				<aside class="hc_post-date-type">
					<div class="date entry-date updated">
						<div class="day"><?php  echo get_the_date('d'); ?></div>
						<div class="month-year"><?php the_time('M, Y'); ?></div>
					</div>
				</aside>
				<div class="media-body">
					<h4><a href="<?php the_permalink(); ?>" title="webriti" ><?php the_title(); ?></a></h4>
					<p><?php echo get_home_blog_excerpt(); ?></p>
				</div>
			</div>			
			<?php } endwhile; 
			} else  {
			echo __('No posts to show','health');
			} ?>
</div><!--/Recent News-->