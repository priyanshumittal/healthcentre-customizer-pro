<?php
/*
 * Home page additional section
 */
$current_options = wp_parse_args( get_option('hc_pro_options', array() ), theme_data_setup()); ?>
<?php if($current_options['additional_enable'] == false) { ?>
<div class="row"><div class="col-md-12"><div class="hc_home_border"></div></div></div>
<?php if($current_options['additional_title'] && $current_options['additional_description']!='') { ?>
<div class="row">
	<div class="section-header">
		<?php if($current_options['additional_title']!='') { ?>
		<h1 class="section-title"><?php echo $current_options['additional_title']; ?></h1>
		<?php } ?>
		<?php if($current_options['additional_description']!='') { ?>
		<p class="section-subtitle"><?php echo $current_options['additional_description']; ?></p>
		<?php } ?>		
	</div>
</div>
<?php }
if( is_active_sidebar('sidebar-additional-section') ){ 
?>

<div class="container sidebar-content-bottom">
	<div class="row">
		<?php dynamic_sidebar('sidebar-additional-section'); ?>
	</div>
</div>
<?php } 
} 
?>