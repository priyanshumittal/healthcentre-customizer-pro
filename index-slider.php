<?php/**
* @Theme Name	:	Health-Center
* @file         :	index-slider.php
* @package      :	Health-Center
* @author       :	Hari Maliya
* @license      :	license.txt
* @filesource   :	wp-content/themes/health-center/index-slider.php
*/
?>
<!-- Slider -->
<?php
$current_options = wp_parse_args( get_option('hc_pro_options', array() ), theme_data_setup());
$animation= $current_options['animation'];
$animationSpeed=$current_options['animationSpeed'];
$direction=$current_options['slide_direction'];
$slideshowSpeed=$current_options['slideshowSpeed'];
$home_slider_enabled = $current_options['home_slider_enabled'];
?>
<script>
jQuery(window).load(function() {
  // The slider being synced must be initialized first
  jQuery('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
	directionNav: false,
    slideshow: false,
	itemMargin: 5,
    asNavFor: '#slider'
  });
   
  jQuery('#slider').flexslider({
	animation: "<?php echo $animation; ?>",
	animationSpeed: <?php echo $animationSpeed; ?>,
	direction: "<?php echo $direction; ?>",
	slideshowSpeed: <?php echo $slideshowSpeed; ?>,
    manualControls: ".slide_thumb li",
    useCSS: false,
  });
});
</script>
<?php if($home_slider_enabled == true) {  
if($current_options['slider_category'] !='')
{
	$arg = array( 'post_type'=>'post','category__in' => $current_options['slider_category'],'ignore_sticky_posts' => 1);
	$loop = new WP_Query( $arg );
	if ( $loop->have_posts() ) :
	?>
	<div class="hc_slider">	
		<div id="slider" class="flexslider">
			<ul class="slides">	
			<?php 
			while ( $loop->have_posts() ) : $loop->the_post(); 
				if(has_post_thumbnail()):
			?>
				<li>
					<?php $defalt_arg =array('class' => "img-responsive"); ?>
					<?php the_post_thumbnail('full', $defalt_arg); ?>			
				</li>
			<?php 
				endif;
			endwhile; ?>
			</ul>
		</div>		
		<div id="carousel" class="flexslider">
			<ul class="slides slide_thumb">
				<?php $i=1; 
				while ( $loop->have_posts() ) : $loop->the_post(); 
					if(has_post_thumbnail()):
				?>
				<li  class="<?php if($i==1){ echo 'flex-active';$i=2;}?>">
					<h5><?php the_title(); ?></h5>
					<?php the_excerpt(); ?>
				</li>
				<?php 
					endif;
				endwhile; ?>
			</ul>
		</div>
	</div>	
<?php endif; } else {		
		$current_options = get_option('hc_pro_options');
		if(!empty($current_options)) {
		get_template_part('index','sliderold'); 
		} } }
 ?>
<!-- /Slider -->