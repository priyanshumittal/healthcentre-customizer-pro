<?php
/************* Home slider custom post type ************************/
	$current_options = wp_parse_args( get_option('hc_pro_options', array() ), theme_data_setup());
	$slug_slide = $current_options['hc_slider_slug'];
	$slug_service = $current_options['hc_service_slug'];
	$slug_portfolio = $current_options['hc_portfolio_slug'];
	$slug_team = $current_options['hc_team_slug'];
	$slug_testimonial = $current_options['hc_testimonial_slug'];
	$slug_portfolio_categori = $current_options['hc_testimonial_slug'];
		
function healthcenter_slider() {
	register_post_type( 'healthcenter_slider');
}
add_action( 'init', 'healthcenter_slider' );

/************* Home Service custom post type ***********************/	
function healthcenter_service_type()
{	register_post_type( 'healthcenter_service');
}
add_action( 'init', 'healthcenter_service_type' );

/*Testimonial*/
function health_testimonial() {
	register_post_type( 'health_testimonial');
}
add_action( 'init', 'health_testimonial' );

//************* Home project custom post type ***********************
function healthcenter_portfolio_type()
{	register_post_type( 'healthcenter_project',
		array(
			'labels' => array(
				'name' => __('Portfolio / Project', 'health'),
				//'singular_name' => 'Featured Services',
				'add_new' => __('Add New', 'health'),
				'add_new_item' => __('Add New Portfolio / Project','health'),
				'edit_item' => __('Add New','health'),
				'new_item' => __('New Link','health'),
				'all_items' => __('All Portfolio / Project','health'),
				'view_item' => __('View Link','health'),
				'search_items' => __('Search Links','health'),
				'not_found' =>  __('No Links found','health'),
				'not_found_in_trash' => __('No Links found in Trash','health'), 
			),
			'supports' => array('title','editor','thumbnail'),
			'show_in' => true,
			'show_in_nav_menus' => false,
			'rewrite' => array('slug' => $GLOBALS['slug_portfolio']),
			'public' => true,
			'menu_position' =>20,
			'public' => true,
			'menu_icon' => WEBRITI_TEMPLATE_DIR_URI . '/images/option-icon-media.png',
		)
	);
}
add_action( 'init', 'healthcenter_portfolio_type' );

//************* Team POST TYPE *****************************
function healthcenter_team_type()
{	register_post_type( 'healthcenter_team',
		array(
			'labels' => array(
				'name' => __('Our Team', 'health'),
				//'singular_name' => 'Featured Services',
				'add_new' => __('Add New', 'health'),
                'add_new_item' => __('Add New Team','health'),
				'edit_item' => __('Add New','health'),
				'new_item' => __('New Link','health'),
				'all_items' => __('All Teams','health'),
				'view_item' => __('View Link','health'),
				'search_items' => __('Search Links','health'),
				'not_found' =>  __('No Links found','health'),
				'not_found_in_trash' => __('No Links found in Trash','health'), 
				),
			'supports' => array('title','thumbnail'),
			'show_in' => true,
			'show_in_nav_menus' => false,
			'rewrite' => array('slug' => $GLOBALS['slug_team']),
			'public' => true,
			'menu_position' => 20,
			'public' => true,
			)
	);
}
add_action( 'init', 'healthcenter_team_type' );
?>