<?php
// Icon info widget
function icon_info_widget() {
	register_widget( 'icon_info_widget' );
}
add_action( 'widgets_init', 'icon_info_widget' );

// Creating the widget
	class icon_info_widget extends WP_Widget {
	 
	function __construct() {
		parent::__construct(
			'icon_info_widget', // Base ID
			__('WBR : Icon Info Widget', 'health'), // Name
			array(
				'classname' => 'icon_info_widget',
				'description' => __('Icon Widget','health'),
			),
			array(
				'width' => 600,
			)
		);
		
	 }
	public function widget( $args, $instance ) {
		$instance[ 'icon' ] = isset($instance[ 'icon' ])?$instance[ 'icon' ]:'';
		$instance[ 'icon_info' ] = isset($instance[ 'icon_info' ])?$instance[ 'icon_info' ]:'';
		$instance[ 'link' ] = isset($instance[ 'link' ])?$instance[ 'link' ]:'';
		
		$args['before_widget'] = str_replace('widget','widget pull-right',$args['before_widget']);
		
		echo $args['before_widget']; 
			
			echo '<ul><li><i class="fa '.$instance[ 'icon' ].'"></i>';
			
				if($instance[ 'link' ])
				echo '<a href="'.$instance[ 'link' ].'">';
			
				 echo $instance[ 'icon_info' ];
				 
				if($instance[ 'link' ])
				echo '</a>';
				
			echo '</li></ul>';
			
		echo $args['after_widget'];
	}
	         
	// Widget Backend
	public function form( $instance ) {
	
	$instance[ 'icon' ] = isset($instance[ 'icon' ])?$instance[ 'icon' ]:'';
	$instance[ 'icon_info' ] = isset($instance[ 'icon_info' ])?$instance[ 'icon_info' ]:'';
	$instance[ 'link' ] = isset($instance[ 'link' ])?$instance[ 'link' ]:'';

	// Widget admin form
	?>
	
	<p>
		<h4 for="<?php echo $this->get_field_id( 'icon' ); ?>"><?php _e( 'Font Awesome Icon URL ( like: fa-phone )', 'health' ); ?><a href="http://fontawesome.io/icons/" target="_blank"><?php _e('Get your fontawesome icons.','health'); ?></a></h4>
		<input class="widefat" id="<?php echo $this->get_field_id( 'icon' ); ?>" name="<?php echo $this->get_field_name( 'icon' ); ?>" type="text" value="<?php if($instance[ 'icon' ]) echo esc_attr( $instance[ 'icon' ] ); ?>" />
	</p>
	
	<p>
		<h4 for="<?php echo $this->get_field_id( 'icon_info' ); ?>"><?php _e( 'Icon', 'health' ); ?></h4>
		<input class="widefat" id="<?php echo $this->get_field_id( 'icon_info' ); ?>" name="<?php echo $this->get_field_name( 'icon_info' ); ?>" type="text" value="<?php if($instance[ 'icon_info' ]) echo esc_attr( $instance[ 'icon_info' ] ); ?>" />
	</p>
	
	<p>
		<h4 for="<?php echo $this->get_field_id( 'link' ); ?>"><?php _e( 'Icon URL', 'health' ); ?></h4>
		<input class="widefat" id="<?php echo $this->get_field_id( 'link' ); ?>" name="<?php echo $this->get_field_name( 'link' ); ?>" type="text" value="<?php if($instance[ 'link' ]) echo esc_attr( $instance[ 'link' ] ); ?>" />
	</p>
	
	<?php
    }
	     
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
	
		$instance = array();
		
		$instance['icon'] = ( ! empty( $new_instance['icon'] ) ) ? strip_tags( $new_instance['icon'] ) : '';
		$instance['icon_info'] = ( ! empty( $new_instance['icon_info'] ) ) ? $new_instance['icon_info'] : '';
		$instance['link'] = ( ! empty( $new_instance['link'] ) ) ? $new_instance['link'] : '';
		
		return $instance;
	}
	}