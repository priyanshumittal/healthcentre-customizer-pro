<?php
/**
 * Register team section widget
 *
 */
add_action('widgets_init','wbr_team_section_widget');
function wbr_team_section_widget(){
	
	return register_widget('wbr_team_section_widget');
}

class wbr_team_section_widget extends WP_Widget{
	
	function __construct() {
		parent::__construct(
			'wbr_team_section_widget', // Base ID
			__('WBR : Team Widget','health'), // Name
			array( 'description' => __('Team item widget', 'health' ), ) // Args
		);
	}
	
	public function widget( $args , $instance ) {
		
			echo $args['before_widget'];
			$selected_team_id=(isset($instance['selected_team_id'])?$instance['selected_team_id']:'');
			if(($instance['selected_team_id']) !=null) {
			$page_data=get_post($selected_team_id);
			?>
			<div class="hc_team_showcase">
				<?php echo get_the_post_thumbnail( $selected_team_id, 'full' ); ?>				
			<div class="caption">
				<h3><?php echo $page_data->post_title; ?></h3>
				
				<?php $designation=get_post_meta( $selected_team_id, 'meta_designation', true ); ?>
				<h6><?php if (!empty($designation)) echo esc_attr($designation); ?></h6>
				
				<?php $description=get_post_meta( $selected_team_id, 'meta_description', true ); ?>
				<p><?php if (!empty($description)) echo $description; ?></p>
				
				<div class="hc_aboutus1_team_social">
				<?php
					$fb_url_cb = get_post_meta( $selected_team_id, 'meta_fb_url_cb', true );
					$twt_url_cb = get_post_meta( $selected_team_id, 'meta_twt_url_cb', true );
					$lnkd_url_cb = get_post_meta( $selected_team_id, 'meta_lnkd_url_cb', true );
					$google_url_cb = get_post_meta( $selected_team_id, 'meta_google_url_cb', true );
					$fb_url = get_post_meta( $selected_team_id, 'meta_fb_url', true );
					$twt_url =  get_post_meta( $selected_team_id, 'meta_twt_url', true );
					$lnkd_url = get_post_meta( $selected_team_id, 'meta_lnkd_url', true );
					$google_url =get_post_meta( $selected_team_id, 'meta_google_url', true );					
					?>
					<?php if($fb_url_cb==1): ?>
					<a href="<?php if(isset($fb_url)){ echo esc_html($fb_url);} else { echo "http://facebook.com"; } ?>" target="_blank" id="fb_tooltip" class="facebook">&nbsp;</a>
					<?php endif; ?>
					<?php if($twt_url_cb==1): ?>
					<a href="<?php if(isset($twt_url)){ echo esc_html($twt_url);} else { echo "http://twitter.com"; } ?>" target="_blank" id="twi_tooltip" class="twitter">&nbsp;</a>
					<?php endif; ?>
					<?php if($lnkd_url_cb==1): ?>
					<a href="<?php if(isset($lnkd_url)){ echo esc_html($lnkd_url);} else { echo "http://linkeding.com"; } ?>" target="_blank" id="in_tooltip" class="linked-in">&nbsp;</a>
					<?php endif; ?>
					<?php if($google_url_cb==1): ?>
					<a href="<?php if(isset($google_url)){ echo esc_html($google_url);} else { echo "http://google.com"; } ?>" target="_blank" id="plus_tooltip" class="google_plus">&nbsp;</a>
					<?php endif; ?>				
				</div>
			</div>
			</div>
			
			<?php }			
		echo $args['after_widget'];
	}
	
		public function form( $instance ) {
		$instance['selected_team_id'] = isset($instance['selected_team_id']) ? $instance['selected_team_id'] : '';
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'selected_team_id' ); ?>"><?php _e('Select team mamber','health'); ?></label> 
			<select class="widefat" id="<?php echo $this->get_field_id( 'selected_team_id' ); ?>" name="<?php echo $this->get_field_name( 'selected_team_id' ); ?>">
				<option value>--<?php echo __('Select','health'); ?>--</option>
				<?php
					$selected_team_id = $instance['selected_team_id'];
					$args = array(
					'post_type' => 'healthcenter_team',
					);
					$loop = new WP_Query($args);

					while($loop->have_posts()): $loop->the_post();
						$option = '<option value="' . get_the_ID() . '" ';
						$option .= ( get_the_ID() == $selected_team_id ) ? 'selected="selected"' : '';
						$option .= '>';
						$option .= get_the_title();
						$option .= '</option>';
						echo $option;
					endwhile;
					wp_reset_query();
				?>	
			</select>
			<br/>
		</p>
		<?php  ?>
        </select>
	<?php 
	}
	
	public function update( $new_instance, $old_instance ) {
		
		$instance = array();
		$instance['selected_team_id'] = ( ! empty( $new_instance['selected_team_id'] ) ) ? $new_instance['selected_team_id'] : '';
		
		return $instance;
	}
}