<?php
	// Register and load the widget
	function faq_health_widget() {
	    register_widget( 'health_faq_widget' );
	}
	add_action( 'widgets_init', 'faq_health_widget' );

// Creating the widget
	class health_faq_widget extends WP_Widget {
	 
	function __construct() {
		parent::__construct(
			'faq_health_widget', // Base ID
			__('WBR : Faq Widget', 'health'), // Name
			array(
				'classname' => 'health_faq_widget',
				'title' => __('Our Mission ?','health'),
				'description' => __('FAQ Widget','health'),
			),
			array(
				'width' => 600,
			)
		);
		
	 }
	public function widget( $args, $instance ) {
	$instance[ 'title' ] = isset($instance[ 'title' ])?$instance[ 'title' ]:'';
	$instance[ 'description' ] = isset($instance[ 'description' ])?$instance[ 'description' ]:'';		
	echo $args['before_widget']; 
	?>
	<div class="hc_panel panel-default">
		<div class="hc_panel-heading">
		  <h4 class="panel-title">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $args['widget_id']; ?>">
			  <span class="fa fa-plus"></span>
			  <?php echo $instance[ 'title' ]; ?>
			</a>
		  </h4>
		</div>
		<div id="collapse<?php echo $args['widget_id']; ?>" class="panel-collapse collapse">
			<div class="panel-body">
				<p><?php echo  $instance[ 'description' ];?></p>
			</div>
		</div>
	</div>					
	<?php
	echo $args['after_widget'];
	}
	         
	// Widget Backend
	public function form( $instance ) {
		
	$instance[ 'title' ] = isset($instance[ 'title' ])?$instance[ 'title' ]:'';
	$instance[ 'description' ] = isset($instance[ 'description' ])?$instance[ 'description' ]:'';	
	
	

	// Widget admin form
	?>
	
	<h4 for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title','health' ); ?></h4>
	<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php if($instance[ 'title' ]) echo esc_attr( $instance[ 'title' ] );?>" />
	<h4 for="<?php echo $this->get_field_id( 'description' ); ?>"><?php _e( 'Description','health' ); ?></h4>
	<input class="widefat" id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>" type="text" value="<?php if($instance[ 'description' ]) echo esc_attr($instance[ 'description' ]);?>" /><br><br>
	<?php
    }
	     
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
	
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['description'] = ( ! empty( $new_instance['description'] ) ) ? $new_instance['description'] : '';
		
		return $instance;
	}
	}