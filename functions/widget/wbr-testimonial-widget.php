<?php
/**
 * Register testimonial section widget
 *
 */
add_action('widgets_init','wbr_testimonial_section_widget');
function wbr_testimonial_section_widget(){
	
	return register_widget('wbr_testimonial_section_widget');
}

class wbr_testimonial_section_widget extends WP_Widget{
	
	function __construct() {
		parent::__construct(
			'wbr_testimonial_section_widget', // Base ID
			__('WBR : Testimonial Widget','health'), // Name
			array( 'description' => __( 'Testimonial section widget','health' ), ) // Args
		);
	}
	
	public function widget( $args , $instance ) {
		$ExcId = array();
		
		echo $args['before_widget'];
		
		$instance['title'] = (isset($instance['title'])?$instance['title']:'');
		$instance['testimoanial_cat'] = (isset($instance['testimoanial_cat'])?$instance['testimoanial_cat']:'');
		$instance['exclude_posts'] = (isset($instance['exclude_posts'])?$instance['exclude_posts']:'');
		$instance['testimoanial_effect'] = (isset($instance['testimoanial_effect'])?$instance['testimoanial_effect']:'');
		$instance['testimoanial_speed'] = (isset($instance['testimoanial_speed'])?$instance['testimoanial_speed']:'');
		if($instance['exclude_posts']!=null){ $ExcId = explode(',',$instance['exclude_posts']);	}
		
		$query_args = array(
		'cat'=> $instance['testimoanial_cat'],
		'ignore_sticky_posts' => 1, 
		'tax_query' => array( array( 'taxonomy' => 'post_format', 'field' => 'slug','terms' => array( 'post-format-quote' ) )),
		'post__not_in' =>$ExcId
		);
		
		$testi_loop = new WP_Query($query_args); 
		
		if( $testi_loop->have_posts() ) :
		?>
		<!-- Testimonial Section -->
			<div class="row">
				<div class="col-md-12">
					<div class="hc_heading_title">
						<?php 
						echo $args['before_title'];
							echo $instance['title'];
						echo $args['after_title'];
						?>
						
						
						<?php if( $testi_loop->post_count > 1 ) { ?>
						<div class="hc_carousel-navi">
							<a id="prev3" class="hc_carousel-prev" href="#mytestimonial-<?php echo $args['widget_id']; ?>" data-slide="prev"><i class="fa fa-angle-left"></i></a>
							<a id="next3" class="hc_carousel-next" href="#mytestimonial-<?php echo $args['widget_id']; ?>" data-slide="next"><i class="fa fa-angle-right"></i></a>
							
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		<div id="mytestimonial-<?php echo $args['widget_id']; ?>" class="carousel slide <?php if($instance['testimoanial_effect'] == 'fade') { echo 'carousel-fade'; } ?>" data-ride="carousel" data-type="multi" >
			<div class="carousel-inner">	
			<?php 
			$i=1;
			while ( $testi_loop->have_posts() ) : $testi_loop->the_post(); 
			?>
				<div class="item <?php if($i==1) { echo 'active';} $i++; ?>">	
				
					<div class="col-md-6 col-sm-6 col-xs-12 hc_testimonials_area">
					
						<div class="hc_testimonials_area_content">
						
							<?php echo custom_excerpt(30,'...','testimonial'); ?>				
						</div>
						
						<div class="hc_testimonials_area_content_bottom_arrow ">
						
							<div class="inner_area"></div>
							
						</div>
						<div class="hc_testimonials_user">
						
							<?php if( has_post_thumbnail() ): ?>
							<div class="hc_testimonials_avatar_wrapper">
							
								<div class="hc_testimonials_avatar">
								<?php the_post_thumbnail( 'thumbnail', array( 'class' => 'img-responsive','style'=>'height:100%;' ) )?>
								</div>
								
							</div>
							<?php endif; ?>
							
							<h4 class="hc_testimonials_title">
							
								<?php the_title(); ?>
								
								<?php if( get_post_meta(get_the_ID(),'designation', true ) ): ?>
								<br/><small><?php echo get_post_meta( get_the_ID(),'designation', true); ?></small>
								<?php endif; ?>
								
							</h4>
							<!--<div class="hc_testimonials_position"></div>-->
							
						</div>
						
					</div>
					
				</div>	
			<?php endwhile;	?>
			</div>
		</div>	
			
		<script>
				jQuery(function($) {

					//	Testimonial Scroll Js	
					
					$('#mytestimonial-<?php echo $args['widget_id']; ?>').carousel({
					  interval: <?php echo $instance['testimoanial_speed']; ?>
					})

					$('#mytestimonial-<?php echo $args['widget_id']; ?> .item').each(function(){
							
					  var next = $(this).next();
					  if (!next.length) {
						next = $(this).siblings(':first');
					  }
					  next.children(':first-child').clone().appendTo($(this));
					  
					  for (var i=0;i<0;i++) {
						next=next.next();
						if (!next.length) {
							next = $(this).siblings(':first');
						}
						
						next.children(':first-child').clone().appendTo($(this));
					  }
					});
					
						
				});
			</script>
		<?php 	
		endif;
		
		echo $args['after_widget'];
	}
	
		public function form( $instance ) {
		$instance['title'] = ( isset($instance['title'] ) ? $instance['title'] :' ');
		$instance['testimoanial_cat'] = (isset($instance['testimoanial_cat'])?$instance['testimoanial_cat']:1);
		$instance['exclude_posts'] = (isset($instance['exclude_posts'])?$instance['exclude_posts']:'');
		$instance['testimoanial_effect'] = (isset($instance['testimoanial_effect'])?$instance['testimoanial_effect']:'slide');
		$instance['testimoanial_speed'] = (isset($instance['testimoanial_speed'])?$instance['testimoanial_speed']:2000);
		?>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title', 'health' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'testimoanial_cat' ); ?>"><?php _e('Select posts category','health' ); ?></label><br/>
			<select id="<?php echo $this->get_field_id( 'testimoanial_cat' ); ?>" name="<?php echo $this->get_field_name( 'testimoanial_cat' ); ?>">
				<option value>--<?php echo __('Select category','health'); ?>--</option>
				<?php 
					$args = array("hide_empty" => 0,
                    "type"      => "post",      
                    "orderby"   => "name",
                    "order"     => "ASC" );
					$cats = get_categories($args);

					foreach ( $cats as $cat )
					{
						printf('<option value="%s" %s>%s</option>', $cat->term_id, selected($instance['testimoanial_cat'], $cat->term_id, false), $cat->name);
					}
				?>
			</select>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'exclude_posts' ); ?>"><?php _e( 'Exclude posts id like ( 1,2,3...etc )','health' ); ?></label> 
			<textarea rows="5" class="widefat" id="<?php echo $this->get_field_id( 'exclude_posts' ); ?>" name="<?php echo $this->get_field_name( 'exclude_posts' ); ?>"><?php if($instance['exclude_posts']) echo $instance['exclude_posts']; ?></textarea>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'testimoanial_effect' ); ?>"><?php _e( 'Animation type','health' ); ?></label><br/> 
			<select id="<?php echo $this->get_field_id( 'testimoanial_effect' ); ?>" name="<?php echo $this->get_field_name( 'testimoanial_effect' ); ?>">
				<option value>--<?php echo __('Select animation','health'); ?>--</option>
				<option value="slide" <?php echo ($instance['testimoanial_effect']=='slide'?'selected':''); ?>><?php echo __('Slide','health'); ?></option>
				<option value="fade" <?php echo ($instance['testimoanial_effect']=='fade'?'selected':''); ?>><?php echo __('Fade','health'); ?></option>
			</select>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'testimoanial_speed' ); ?>"><?php _e( 'Speed','health' ); ?></label><br/> 
			<select id="<?php echo $this->get_field_id( 'testimoanial_speed' ); ?>" name="<?php echo $this->get_field_name( 'testimoanial_speed' ); ?>">
				<option value>--<?php echo __('Slider speed','health'); ?>--</option>
				<option value="500" <?php echo ($instance['testimoanial_speed']==500?'selected':''); ?>>
				<?php echo '500'; ?></option>
				<option value="1000" <?php echo ($instance['testimoanial_speed']==1000?'selected':''); ?>>
				<?php echo '1000'; ?></option>
				<option value="1500" <?php echo ($instance['testimoanial_speed']==1500?'selected':''); ?>>
				<?php echo '1500'; ?></option>
				<option value="2000" <?php echo ($instance['testimoanial_speed']==2000?'selected':''); ?>>
				<?php echo '2000'; ?></option>
				<option value="2500" <?php echo ($instance['testimoanial_speed']==2500?'selected':''); ?>>
				<?php echo '2500'; ?></option>
				<option value="3000" <?php echo ($instance['testimoanial_speed']==3000?'selected':''); ?>>
				<?php echo '3000'; ?></option>
				<option value="3500" <?php echo ($instance['testimoanial_speed']==3500?'selected':''); ?>>
				<?php echo '3500'; ?></option>
				<option value="4000" <?php echo ($instance['testimoanial_speed']==4000?'selected':''); ?>>
				<?php echo '4000'; ?></option>
			</select>
		</p>
		<?php 
	}
	
	
	public function update( $new_instance, $old_instance ) {
		
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? $new_instance['title'] : ' ';
		$instance['testimoanial_cat'] = ( ! empty( $new_instance['testimoanial_cat'] ) ) ? $new_instance['testimoanial_cat'] : '';
		$instance['exclude_posts'] = ( ! empty( $new_instance['exclude_posts'] ) ) ? $new_instance['exclude_posts'] : '';
		$instance['testimoanial_effect'] = ( ! empty( $new_instance['testimoanial_effect'] ) ) ? strip_tags( $new_instance['testimoanial_effect'] ) : '';
		$instance['testimoanial_speed'] = ( ! empty( $new_instance['testimoanial_speed'] ) ) ? strip_tags( $new_instance['testimoanial_speed'] ) : '';
		
		return $instance;
	}
}