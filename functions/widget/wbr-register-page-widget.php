<?php
/**
 * Feature Page Widget
 *
 */
add_action('widgets_init','wbr_feature_page_widget');
function wbr_feature_page_widget(){
	
	return register_widget('wbr_feature_page_widget');
}

class wbr_feature_page_widget extends WP_Widget{
	
	function __construct() {
		parent::__construct(
			'wbr_feature_page_widget', // Base ID
			__('WBR : Page / Service Widget', 'health'), // Name
			array( 'description' => __( 'Feature Page item widget', 'health'), ) // Args
		);
	}
	
	
	public function widget( $args , $instance ) {
		
		echo $args['before_widget'];
			$instance['selected_page'] = (isset($instance['selected_page'])?$instance['selected_page']:'');
			$instance['hide_image'] = (isset($instance['hide_image'])?$instance['hide_image']:'');
			$instance['below_title'] = (isset($instance['below_title'])?$instance['below_title']:'');
			$instance['buttontext'] = (isset($instance['buttontext'])?$instance['buttontext']:'');
			$instance['buttonlink'] = (isset($instance['buttonlink'])?$instance['buttonlink']:'');
			$instance['icon'] = (isset($instance['icon'])?$instance['icon']:'');
			$instance['target'] = (isset($instance['target'])?$instance['target']:'');
			
			// Check for custom link
			
			
			// fetch page object
			$page= get_post($instance['selected_page']);
			
			$title = $page->post_title;
			
	if($instance['selected_page']!=''):
		
		
			    if( ($instance['below_title']) == true ):
			
				        if(($instance['buttonlink'])!=null) echo '<h2><a href="'.$instance['buttonlink'].'" '.($instance['target']==true?'target="_blank"':'').' >'. $title .'</a></h2>';
		                else echo '<h2 class="widget-title">'. $title .'</h2>'; 
							
							
							
			
			    endif;
			
				
				if(($instance['buttonlink'])!=null){
					echo '<a href="'.$instance['buttonlink'].'" '.($instance['target']==true?'target="_blank"':'').' title="'. $title .'">';
				} 
				
				if(($instance['icon'])!=null){
					
							echo '<i class="'. $instance['icon'] .'"></i>';
					
				}
					
			    if( $instance['hide_image'] != true ):
				
				        echo '<div class="portfolio-img">';
					        $attr = array( 'class' => 'img-responsive' );
						    echo get_the_post_thumbnail($page->ID, 'large', $attr);
				        echo '</div>';
						
				endif;
		
			   
				
				if( ($instance['buttonlink'])!=null ){ echo '</a>'; }
				
			
			    if( ($instance['below_title']) != true ):
			
						
							if(($instance['buttonlink'])!=null) echo '<h2><a href="'.$instance['buttonlink'].'" '.($instance['target']==true?'target="_blank"':'').' >'. $title .'</a></h2>';
		                 	else echo '<h2 class="widget-title">'. $title .'</h2>'; 
							
			
			    endif;			 
				    

				
				    if($page->post_excerpt) echo '<p>'.$page->post_excerpt. '</p>';
					else echo '<p>'.$page->post_content. '</p>';
					
								
				
			    if(($instance['buttonlink'])!=null){
					
                echo '<p>
					
					    <a href="'.$instance['buttonlink'].'" '.($instance['target']==true?'target="_blank"':'').' >'.$instance['buttontext'].'
					    </a>
					    
					</p>';
						
				}	
			    else { echo '<p>'.$instance['buttontext'].'</p>';}
				
			       			
		
	endif;	
			
			
		echo $args['after_widget']; 	
	}
	
	public function form( $instance ) {
		$instance['selected_page'] = isset($instance['selected_page']) ? $instance['selected_page'] : '';
		$instance['hide_image'] = isset($instance['hide_image']) ? $instance['hide_image'] : '';
		$instance['below_title'] = isset($instance['below_title']) ? $instance['below_title'] : '';
		$instance['buttontext'] = isset($instance['buttontext']) ? $instance['buttontext'] : '';
		$instance['buttonlink'] = isset($instance['buttonlink']) ? $instance['buttonlink'] : '';
		$instance['icon'] = isset($instance['icon']) ? $instance['icon'] : '';
		$instance['target'] = isset($instance['target']) ? $instance['target'] : '';
		
		
		?>
		
		
	    <p>
		<label for="<?php echo $this->get_field_id( 'icon' ); ?>"><?php _e( 'Specify Font Awesome icon class ( like: fa fa-shield ). <a href="//fontawesome.io/icons/" target="blank" >Browse all icons </a>','health' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'icon' ); ?>" name="<?php echo $this->get_field_name( 'icon' ); ?>" type="text" value="<?php echo esc_attr( $instance['icon'] ); ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'selected_page' ); ?>"><?php _e('Select pages','health' ); ?></label> 
			<select class="widefat" id="<?php echo $this->get_field_id( 'selected_page' ); ?>" name="<?php echo $this->get_field_name( 'selected_page' ); ?>">
				<option value>--<?php echo __('Select','health'); ?>--</option>
				<?php
					$selected_page = $instance['selected_page'];
					$pages = get_pages($selected_page); 				
					foreach ( $pages as $page ) {
						$option = '<option value="' . $page->ID . '" ';
						$option .= ( $page->ID == $selected_page ) ? 'selected="selected"' : '';
						$option .= '>';
						$option .= $page->post_title;
						$option .= '</option>';
						echo $option;
					}
				?>
						
			</select>
			<br/>
			
		</p>
		
		<p>
		<input class="checkbox" type="checkbox" <?php if($instance['hide_image']==true){ echo 'checked'; } ?> id="<?php echo $this->get_field_id( 'hide_image' ); ?>" name="<?php echo $this->get_field_name( 'hide_image' ); ?>" /> 
		<label for="<?php echo $this->get_field_id( 'hide_image' ); ?>"><?php _e( 'Hide featured image','health' ); ?></label>
		</p>
		
		<p>
		<input class="checkbox" type="checkbox" <?php if($instance['below_title']==true){ echo 'checked'; } ?> id="<?php echo $this->get_field_id( 'below_title' ); ?>" name="<?php echo $this->get_field_name( 'below_title' ); ?>" /> 
		<label for="<?php echo $this->get_field_id( 'below_title' ); ?>"><?php _e( 'Display image / icon below title','health' ); ?></label>
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'buttontext' ); ?>"><?php _e( 'Button Text','health' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'buttontext' ); ?>" name="<?php echo $this->get_field_name( 'buttontext' ); ?>" type="text" value="<?php echo esc_attr( $instance['buttontext'] ); ?>" />
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'buttonlink' ); ?>"><?php _e( 'Button Link','health' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'buttonlink' ); ?>" name="<?php echo $this->get_field_name( 'buttonlink' ); ?>" type="text" value="<?php echo esc_attr( $instance['buttonlink'] ); ?>" />
		</p>
		
		<p>
		<input class="checkbox" type="checkbox" <?php if($instance['target']==true){ echo 'checked'; } ?> id="<?php echo $this->get_field_id( 'target' ); ?>" name="<?php echo $this->get_field_name( 'target' ); ?>" /> 
		<label for="<?php echo $this->get_field_id( 'target' ); ?>"><?php _e( 'Open link in new tab','health' ); ?></label>
		</p>
		
		
      
		<?php  ?>
        </select>
		
		
		<?php 
	}
	
	public function update( $new_instance, $old_instance ) {
		
		$instance = array();
		$instance['selected_page'] = ( ! empty( $new_instance['selected_page'] ) ) ? $new_instance['selected_page'] : '';
		$instance['hide_image'] = ( ! empty( $new_instance['hide_image'] ) ) ? $new_instance['hide_image'] : '';
		$instance['below_title'] = ( ! empty( $new_instance['below_title'] ) ) ? $new_instance['below_title'] : '';
		$instance['buttontext'] = ( ! empty( $new_instance['buttontext'] ) ) ? $new_instance['buttontext'] : '';
		$instance['buttonlink'] = ( ! empty( $new_instance['buttonlink'] ) ) ? $new_instance['buttonlink'] : '';
		$instance['icon'] = ( ! empty( $new_instance['icon'] ) ) ? $new_instance['icon'] : '';
		$instance['target'] = ( ! empty( $new_instance['target'] ) ) ? $new_instance['target'] : '';
		
		return $instance;
	}
}