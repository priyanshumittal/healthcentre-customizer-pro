<?php	
add_action( 'widgets_init', 'webriti_widgets_init');
function webriti_widgets_init() {
	
	$current_options = wp_parse_args( get_option('hc_pro_options', array() ), theme_data_setup());
	$service_layout = $current_options['service_column_layout'];
	$service_layout = 12 / $service_layout;
	
	$project_layout = $current_options['project_column_layout'];
	$project_layout = 12 / $project_layout;
	
	$additional_layout = $current_options['additional_column_layout'];
	$additional_layout = 12 / $additional_layout;
	
	
	
	/*sidebar*/
	register_sidebar( array(
		'name' => __('Sidebar widget area', 'health' ),
		'id' => 'sidebar-primary',
		'description' => __('Sidebar widget area','health' ),
		'before_widget' => '<div id="%1$s" class="widget %1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="widget-title">',
		'after_title' => '</h2>',
	) );
	
	/* Top header widget area */
	register_sidebar( array(
		'name' => __( 'Top header sidebar', 'health' ),
		'id' => 'sidebar-header',
		'description' => __( 'Top header widget area', 'health' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	// Service Widget Sidebar	
	register_sidebar( array(
		'name' => __( 'Homepage service section - sidebar ', 'health' ),
		'id' => 'sidebar-service',
		'description' => __('Use the widget WBR: Page/Service Widget to add service type content','health'),
		'before_widget' => '<div id="%1$s" class="col-md-'.$service_layout.' hc_service_area widget %1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );


	//Project Sidebar
	register_sidebar( array(
			'name' => __( 'Homepage project section - sidebar', 'health' ),
			'id' => 'sidebar-project',
			'description' => __('Use the widget WBR: Project Widget to add project type content','health'),
			'before_widget' => '<div id="%1$s" class="col-md-'.$project_layout.' widget %1$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		) );
	
	// Home Page Latest news Sidebar
	register_sidebar( array(
		'name' => __( 'Homepage latest news section - sidebar', 'health' ),
		'id' => 'sidebar-news',
		'description' => __('Use the widget WBR: Recent News Widget to add Recent News type post','health'),
		'before_widget' => '<div id="%1$s" class="hc_post_section widget %1$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="hc_heading_title"><h3 class="widget-title">',
		'after_title' => '</h3></div>',
	) );
	
	
	// Home Page Faqs Sidebar
	register_sidebar( array(
		'name' => __( 'Homepage faq section - sidebar', 'health' ),
		'id' => 'sidebar-faq',
		'description' => __('Use the widget WBR: Faq Widget to add Faq type content','health'),
		'before_widget' => '<div id="%1$s" class="widget %1$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="hc_heading_title"><h3 class="widget-title">',
		'after_title' => '</h3></div>',
	) );
	
	//Testimonial Sidebar
	register_sidebar( array(
		'name' => __( 'Homepage testimonial section - sidebar', 'health' ),
		'id' => 'sidebar-testimonial',
		'description' => __('Use the widget WBR: Testimonial Widget to add Testimonial','health'),
		'before_widget' => '<div id="%1$s" class="container testimonial-section widget %1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	// Home Page Additional Sidebar
	register_sidebar( array(
		'name' => __( 'Home Page additional section above testimonial', 'health' ),
		'id' => 'sidebar-additional-section',
		'before_widget' => '<div id="%1$s" class="col-md-'.$additional_layout.' widget %1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Contact Page Sidebar
	register_sidebar( array(
		'name' => __( 'Contact Page Sidebar', 'health' ),
		'id' => 'sidebar-contact',
		'description' => __( 'Contact Page Sidebar', 'health' ),
		'before_widget' => '<div id="%1$s" class="hc_contactv1_address widget %1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );
	
	//Footer Widget
	register_sidebar( array(
		'name' => __( 'Footer widget area', 'health' ),
		'id' => 'footer-widget-area',
		'description' => __( 'Footer widget area', 'health' ),
		'before_widget' => '<div id="%1$s" class="col-md-3 hc_footer_widget_column widget %1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="widget-title">',
		'after_title' => '</h2>',
	) );
	
	//Footer Right Sidebar
	register_sidebar( array(
		'name' => __( 'Footer right section area', 'health' ),
		'id' => 'footer-right-section',
		'description' => __( 'Footer right section area', 'health' ),
		'before_widget' => '<div id="%1$s" class="widget %1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	
	
}