<?php
/**
 * Feature Page Widget
 *
 */
add_action('widgets_init','wbr_project_widget');
function wbr_project_widget(){
	
	return register_widget('wbr_project_widget');
}

class wbr_project_widget extends WP_Widget{
	
	function __construct() {
		parent::__construct(
			'wbr_project_widget', // Base ID
			__('WBR : Project Widget', 'health'), // Name
			array( 'description' => __( 'Project item widget', 'health'), ) // Args
		);
	}
	
	
		public function widget( $args, $instance ) {
		echo $args['before_widget'];
		$selected_post_id=(isset($instance['selected_post_id'])?$instance['selected_post_id']:'');
		if(($instance['selected_post_id']) !=null) {
		$page_data=get_post($selected_post_id);
		$post_thumbnail_id = get_post_thumbnail_id($selected_post_id);
		$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
		$link=1;
		$meta_project_link = '';
		if(get_post_meta( $selected_post_id,'meta_project_link', true )) 
		{ $meta_project_link=get_post_meta( $selected_post_id,'meta_project_link', true ); }
		else { 	    $link=0; }
		
		
		?>
			<div class="hc_home_portfolio_showcase">
				<div class="hc_home_portfolio_showcase_media">
					<?php echo get_the_post_thumbnail( $selected_post_id, 'full' ); ?>
					<div class="hc_home_portfolio_showcase_overlay">
						<div class="hc_home_portfolio_showcase_overlay_inner">
							<div class="hc_home_portfolio_showcase_icons">
								<a title="<?php _e('Health Center','health'); ?>" href="<?php echo $meta_project_link; ?>" <?php  if(get_post_meta( $selected_post_id,'meta_project_target', true )) { echo "target='_blank'"; }  ?>><i class="fa fa-link"></i></a>
								<a href="<?php echo $post_thumbnail_url; ?>"  data-lightbox="image" title="<?php the_title(); ?>" class="hover_thumb"><i class="fa fa-picture-o"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="hc_home_portfolio_caption">
			    <?php if ($link==1 ) { ?>
				<h3><a href="<?php echo $meta_project_link; ?>" <?php  if(get_post_meta( $selected_post_id,'meta_project_target', true )) { echo "target='_blank'"; }  ?>><?php echo $page_data->post_title; ?></a></h3>
				<?php } else { ?><h3><?php echo $page_data->post_title; ?> </h3> <?php } ?>
				<small><?php echo get_post_meta( $selected_post_id,'portfolio_project_summary', true ); ?></small>
			</div>
		<?php }
	echo $args['after_widget'];
	}
	
	public function form( $instance ) {
		$instance['selected_post_id'] = isset($instance['selected_post_id']) ? $instance['selected_post_id'] : '';
		?>
		
		
	    

		<p>
			<label for="<?php echo $this->get_field_id( 'selected_post_id' ); ?>"><?php _e( 'Select Project Items','health' ); ?></label> 
			<select class="widefat" id="<?php echo $this->get_field_id( 'selected_post_id' ); ?>" name="<?php echo $this->get_field_name( 'selected_post_id' ); ?>">
				<option value>--<?php echo __('Select','health'); ?>--</option>
				<?php
					$selected_post_id = $instance['selected_post_id'];
					$args = array(
					'post_type' => 'healthcenter_project',
					);
					$loop = new WP_Query($args);

					while($loop->have_posts()): $loop->the_post();
						$option = '<option value="' . get_the_ID() . '" ';
						$option .= ( get_the_ID() == $selected_post_id ) ? 'selected="selected"' : '';
						$option .= '>';
						$option .= get_the_title();
						$option .= '</option>';
						echo $option;
					endwhile;
					wp_reset_query();
				?>	
			</select>
			<br/>
		</p>
		<?php  ?>
        </select>
		<?php 
	}
	
	public function update( $new_instance, $old_instance ) {
		
		$instance = array();
		$instance['selected_post_id'] = ( ! empty( $new_instance['selected_post_id'] ) ) ? $new_instance['selected_post_id'] : '';
		
		return $instance;
	}
}