<?php 
$current_options = wp_parse_args( get_option('hc_pro_options', array() ), theme_data_setup());
if($current_options['enable_custom_typography']==true)
{
?>
<style> 
/****** custom typography *********/ 
.hc_blog_post_content p,
.hc_service_area,
.hc_service_area p,
.hc_home_portfolio_caption small,
.hc_post_area p,
.hc_testimonials_area_content,
.hc_footer_area p,
.textwidget,
.hc_contactv1_address,
.hc_team_showcase p,
.hc_portfolio_caption small
{
	font-size:<?php echo $current_options['general_typography_fontsize'].'px'; ?> !important;
	font-family:<?php echo $current_options['general_typography_fontfamily']; ?> !important;
	font-style:<?php echo $current_options['general_typography_fontstyle']; ?> ;
	line-height:<?php echo ($current_options['general_typography_fontsize']+10).'px'; ?> !important;
	
}
/*** Menu title */
.navbar .nav > li > a{
	font-size:<?php echo $current_options['menu_title_fontsize'].'px'; ?>;
	font-family:<?php echo $current_options['menu_title_fontfamily']; ?>;
	font-style:<?php echo $current_options['menu_title_fontstyle']; ?>;
}

/*** post title */
.hc_post_title_wrapper h2,.hc_post_title_wrapper h2 a, .hc_page_header_area h1, .blog_section2 h2,.blog_section2 h2 a, .blog_section h2,.blog_section h2 a, .hc_post_area h4,.hc_post_area h4 a {
	font-size:<?php echo $current_options['post_title_fontsize'].'px'; ?> !important;
	font-family:<?php echo $current_options['post_title_fontfamily']; ?> !important;
	font-style:<?php echo $current_options['post_title_fontstyle']; ?> !important;
}
/*** service title */
.hc_service_area h2 , .hc_service_area h1, .hc_search_head, .hc_service_area .widget-title
{
	font-size:<?php echo $current_options['service_title_fontsize'].'px'; ?> !important;
	font-family:<?php echo $current_options['service_title_fontfamily']; ?> !important;
	font-style:<?php echo $current_options['service_title_fontstyle']; ?> !important;
	line-height:<?php echo ($current_options['service_title_fontsize']+5).'px'; ?> !important;
}

.our_main_ser_title{ 
	font-size:<?php echo $current_options['service_title_fontsize'].'px'; ?>;
	font-family:<?php echo $current_options['service_title_fontfamily']; ?>;
	font-style:<?php echo $current_options['service_title_fontstyle']; ?>;
}
/******** portfolio title ********/
.hc_project_header_area h1, .hc_home_portfolio_caption h3,.hc_home_portfolio_caption h3 a{ 
	font-size:<?php echo $current_options['portfolio_title_fontsize'].'px'; ?> !important;
	font-family:<?php echo $current_options['portfolio_title_fontfamily']; ?> !important;
	font-style:<?php echo $current_options['portfolio_title_fontstyle']; ?> !important;
	line-height:<?php echo ($current_options['portfolio_title_fontsize']+5).'px'; ?> !important;
}
.hc_portfolio_caption h3 {
	font-size:<?php echo $current_options['portfolio_title_fontsize'].'px'; ?>;
	font-family:<?php echo $current_options['portfolio_title_fontfamily']; ?>;
	font-style:<?php echo $current_options['portfolio_title_fontstyle']; ?>;
}
/******* footer widget title*********/
.hc_footer_widget_title,.widget-title{
	font-size:<?php echo $current_options['widget_title_fontsize'].'px'; ?> !important;
	font-family:<?php echo $current_options['widget_title_fontfamily']; ?> !important;
	font-style:<?php echo $current_options['widget_title_fontstyle']; ?> !important;
	line-height:<?php echo ($current_options['widget_title_fontsize']+5).'px'; ?> !important;
}
.hc_sidebar_widget_title h2, h2.widgettitle{
	font-size:<?php echo $current_options['widget_title_fontsize'].'px'; ?>;
	font-family:<?php echo $current_options['widget_title_fontfamily']; ?>;
	font-style:<?php echo $current_options['widget_title_fontstyle']; ?>;
}
.hc_home_title h1{
	font-size:<?php echo $current_options['calloutarea_title_fontsize'].'px'; ?>;
	font-family:<?php echo $current_options['calloutarea_title_fontfamily']; ?>;
	font-style:<?php echo $current_options['calloutarea_title_fontstyle']; ?>;
}
.hc_callout_area h1{
	font-size:<?php echo $current_options['calloutarea_description_fontsize'].'px'; ?> !important;
	font-family:<?php echo $current_options['calloutarea_description_fontfamily']; ?> !important;
	font-style:<?php echo $current_options['calloutarea_description_fontstyle']; ?> !important;
	line-height:<?php echo ($current_options['calloutarea_description_fontsize']+5).'px'; ?> !important;
}
.hc_callout_area a {	
	font-size:<?php echo $current_options['calloutarea_purches_fontsize'].'px'; ?> !important;
	font-family:<?php echo $current_options['calloutarea_purches_fontfamily']; ?> !important;
	font-style:<?php echo $current_options['calloutarea_purches_fontstyle']; ?> !important;
	line-height:<?php echo ($current_options['calloutarea_purches_fontsize']+5).'px'; ?> !important;
}

/*Section Title */

.section-title {	
	font-size:<?php echo $current_options['section_title_fontsize'].'px'; ?> !important;
	font-family:<?php echo $current_options['section_title_fontfamily']; ?> !important;
	font-style:<?php echo $current_options['section_title_fontstyle']; ?> !important;
	line-height:<?php echo ($current_options['section_title_fontsize']+4).'px'; ?> !important;
}

/*Section Sub-title */

.section-subtitle {	
	font-size:<?php echo $current_options['section_subtitle_fontsize'].'px'; ?> !important;
	font-family:<?php echo $current_options['section_subtitle_fontfamily']; ?> !important;
	font-style:<?php echo $current_options['section_subtitle_fontstyle']; ?> !important;
	line-height:<?php echo ($current_options['section_subtitle_fontsize']+5).'px'; ?> !important;
}

</style>
<?php } ?>