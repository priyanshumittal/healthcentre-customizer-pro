<?php
function webriti_scripts()
{	// Theme Css 

	$current_options = wp_parse_args( get_option('hc_pro_options', array() ), theme_data_setup());
	wp_enqueue_style('health-style', get_stylesheet_uri() );
	if($current_options['link_color_enable'] == true) {
		health_custom_skin_color();
		}
	else
	{
		$class=$current_options['hc_stylesheet'];
		wp_enqueue_style('default', WEBRITI_TEMPLATE_DIR_URI . '/css/'.$class);
	}
	wp_enqueue_style('responsive', WEBRITI_TEMPLATE_DIR_URI . '/css/media-responsive.css');
	wp_enqueue_style('font', WEBRITI_TEMPLATE_DIR_URI . '/css/font/font.css');
	wp_enqueue_style('tooltips', WEBRITI_TEMPLATE_DIR_URI . '/css/css-tooltips.css');
	//wp_enqueue_style('bootstrap', WEBRITI_TEMPLATE_DIR_URI . '/css/bootstrap.css');	
	wp_enqueue_style('font-awesome', WEBRITI_TEMPLATE_DIR_URI . '/css/font-awesome/css/font-awesome.min.css');
	//Switcher
	wp_enqueue_style('switcher', WEBRITI_TEMPLATE_DIR_URI . '/css/switcher/switcher.css');	
	
	// Bootstrap Js 
	wp_enqueue_script('menu', WEBRITI_TEMPLATE_DIR_URI .'/js/menu/menu.js',array('jquery'));
	wp_enqueue_script('bootstrap_min', WEBRITI_TEMPLATE_DIR_URI .'/js/bootstrap.min.js');

	//Switcher Js
	wp_enqueue_script('switcher',WEBRITI_TEMPLATE_DIR_URI.'/js/switcher/switcher.js');	
	
	if(is_home() || is_page_template('template-frontpage.php') || is_page_template('template-homepage-two.php'))
	{	
		wp_enqueue_style('lightbox', WEBRITI_TEMPLATE_DIR_URI . '/css/lightbox.css');
		wp_enqueue_style('flexslider', WEBRITI_TEMPLATE_DIR_URI . '/css/flexslider/flexslider.css');	
		wp_enqueue_script('lightbox-js', WEBRITI_TEMPLATE_DIR_URI .'/js/lightbox/lightbox-2.6.min.js');
		wp_enqueue_script('jquery-flexslider-js', WEBRITI_TEMPLATE_DIR_URI .'/js/flexslider/jquery.flexslider.js');
		wp_enqueue_script('accordion-tab', WEBRITI_TEMPLATE_DIR_URI .'/js/accordion-tab.js');
		wp_enqueue_script('collapse', WEBRITI_TEMPLATE_DIR_URI .'/js/collapse.js');
	
	}
	
	if(is_page_template('healthcenter_project') == get_post_type())
	{
		
		wp_enqueue_style('lightbox', WEBRITI_TEMPLATE_DIR_URI . '/css/lightbox.css');
		wp_enqueue_script('lightbox-js', WEBRITI_TEMPLATE_DIR_URI .'/js/lightbox/lightbox-2.6.min.js');
	}
	// Portfolio js and css
	if(is_page_template('template-portfolio2c.php') || is_page_template('template-portfolio3c.php') || is_page_template('template-portfolio4c.php') || is_page_template('single-healthcenter_project.php') || taxonomy_exists( 'portfolio_categories')) {
		wp_enqueue_style('lightbox', WEBRITI_TEMPLATE_DIR_URI . '/css/lightbox.css');
		wp_enqueue_script('lightbox-js', WEBRITI_TEMPLATE_DIR_URI .'/js/lightbox/lightbox-2.6.min.js');
	}
	
	require_once('custom_style.php');
	
}
add_action('wp_enqueue_scripts', 'webriti_scripts');

if ( is_singular() ){ wp_enqueue_script( "comment-reply" );	}

function webriti_custom_enqueue_css()
{	global $pagenow;
	if ( in_array( $pagenow, array( 'post.php', 'post-new.php', 'page-new.php', 'page.php' ) ) ) {
		wp_enqueue_style('meta-box-css', WEBRITI_TEMPLATE_DIR_URI . '/css/meta-box.css');	
	}	
}
add_action( 'admin_print_styles', 'webriti_custom_enqueue_css', 10 );

add_action( 'admin_enqueue_scripts', 'admin_enqueue_script_function' );
function admin_enqueue_script_function()
{
wp_enqueue_style( 'jquery-ui' );
wp_enqueue_style('health-drag-drop',WEBRITI_TEMPLATE_DIR_URI.'/css/drag-drop.css');
wp_enqueue_script('health-jquery-ui-drag' , WEBRITI_TEMPLATE_DIR_URI.'/js/layout-drag-drop.js');
}

add_action('wp_head','head_enqueue_custom_css');
function head_enqueue_custom_css()
{
	require_once( WEBRITI_THEME_FUNCTIONS_PATH .'/scripts/custom_style.php');
	$hc_pro_options=theme_data_setup(); 
	$current_options = wp_parse_args(  get_option( 'hc_pro_options', array() ), $hc_pro_options ); 
	if($current_options['webrit_custom_css']!='') {  ?>
	<style>
	<?php echo $current_options['webrit_custom_css']; ?>
	</style>
<?php 
		} 
}
// footer custom script
function footer_custom_script()
{
	$hc_pro_options=theme_data_setup(); 
	$current_options = wp_parse_args(  get_option( 'hc_pro_options', array() ), $hc_pro_options );
	
	if($current_options['google_analytics']!='') 
	{  ?>
		<script type="text/javascript">
		<?php echo $current_options['google_analytics']; ?>
		</script>
	<?php 
	} 
} 
add_action('wp_footer','footer_custom_script'); 

function health_registers() {

	wp_enqueue_script( 'health_customizer_script', get_template_directory_uri() . '/js/healthcenter_customizer.js', array("jquery"), '20120206', true  );
}
add_action( 'customize_controls_enqueue_scripts', 'health_registers' );

// media upload
function upload_scripts()
 {
	// And let's not forget the script we wrote earlier
	 wp_enqueue_media();	
	 wp_enqueue_script('media-upload');
     wp_enqueue_script('thickbox');
     wp_enqueue_script('upload_media_widget', get_template_directory_uri() . '/js/upload-media.js', array('jquery'));
	 wp_enqueue_style('thickbox');
}
add_action("admin_enqueue_scripts","upload_scripts");


?>