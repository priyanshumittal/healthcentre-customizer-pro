<?php	
/* add shortcode for testimonial */
add_shortcode('HC_testimonial_grid','_testimonial_grid_function');
function _testimonial_grid_function($atts){
	
	$atts = shortcode_atts( array( 'cat' => 1 , 'column' => 1 ) , $atts );
	
	$column = 12 / $atts['column'];
		
		$query_args = array(
		'cat'=> $instance['testimoanial_cat'],
		'ignore_sticky_posts' => 1, 
		'tax_query' => array( array( 'taxonomy' => 'post_format', 'field' => 'slug','terms' => array( 'post-format-quote' ) )),
		'post__not_in' =>$ExcId
		);
		
		$testi_loop = new WP_Query($query_args); 
		
		$output = '';
		
		while ( $testi_loop->have_posts() ) : $testi_loop->the_post();
			$output .= '<div class="col-xs-12 col-sm-12 col-md-'.$column.' hc_testimonials_area">
						<div class="hc_testimonials_area_content">
							'.get_the_excerpt().'				
						</div>
						<div class="hc_testimonials_area_content_bottom_arrow ">
							<div class="inner_area"></div>
						</div>
						<div class="hc_testimonials_user">';
						
						if( has_post_thumbnail() ):
							$output .= '<div class="hc_testimonials_avatar_wrapper">
								<div class="hc_testimonials_avatar">
								'.get_the_post_thumbnail( $page->ID,'thumbnail', array( 'class' => 'img-responsive','style'=>'height:100%;' ) ).'
								</div>
							</div>';
						endif;
							
							$output .= '<h4 class="hc_testimonials_title">'.get_the_title().'</h4>
						</div>
					</div>';
		endwhile;
		
		return $output;
}