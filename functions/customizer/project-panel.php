<?php
// customizer serive panel
function customizer_project_panel( $wp_customize ) {

	//Service panel
	$wp_customize->add_panel( 'project_panel' , array(
	'title'      => __('Project section', 'health'),
	'capability'     => 'edit_theme_options',
	'priority'   => 520,
   	) );
	
		//Service panel
		$wp_customize->add_section( 'project_settings' , array(
		'title'      => __('Settings', 'health'),
		'panel'  => 'project_panel',
		'priority'   => 1,
		) );
			
			// enable project section
			$wp_customize->add_setting('hc_pro_options[project_enable]',array(
			'default' => false,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'type' => 'option'
			) );
			
			$wp_customize->add_control('hc_pro_options[project_enable]',array(
			'label' => __('Hide section','health'),
			'section' => 'project_settings',
			'type' => 'checkbox',
			) );
			
			// Number of services
			$wp_customize->add_setting('hc_pro_options[project_column_layout]',array(
			'default' => 4,
			'type' => 'option',
			'sanitize_callback' => 'sanitize_text_field',
			) );

			$wp_customize->add_control('hc_pro_options[project_column_layout]',array(
			'type' => 'select',
			'label' => __('Select column layout','health'),
			'section' => 'project_settings',
			'choices' => array(2=>'2',3=>'3',4=>'4'),
			) );
		
		// headings
		$wp_customize->add_section( 'project_headings' , array(
		'title'      => __('Section Header', 'health'),
		'panel'  => 'project_panel',
		'priority'   => 2,
		) );
			
			// Project title
			$wp_customize->add_setting('hc_pro_options[portfolio_title]',array(
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'type' => 'option'
			) );
			
			$wp_customize->add_control('hc_pro_options[portfolio_title]',array(
			'label' => __('Title','health'),
			'section' => 'project_headings',
			'type' => 'text',
			) );
			
			// Project description
			$wp_customize->add_setting('hc_pro_options[portfolio_description]',array(
			'sanitize_callback' => 'sanitize_text_field',
			'type' => 'option'
			) );
			
			$wp_customize->add_control('hc_pro_options[portfolio_description]',array(
			'label' => __('Subtitle','health'),
			'section' => 'project_headings',
			'type' => 'textarea',
			'sanitize_callback' => 'sanitize_text_field',
			) );
	
}
add_action( 'customize_register', 'customizer_project_panel' );