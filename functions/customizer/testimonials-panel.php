<?php
// customizer serive panel
function customizer_testimonial_panel( $wp_customize ) {

	//Testimonial panel
	$wp_customize->add_panel( 'testimonial_panel' , array(
	'title'      => __('Testimonial section', 'health'),
	'capability'     => 'edit_theme_options',
	'priority'   => 560,
   	) );
	
		//Service panel
		$wp_customize->add_section( 'testimonial_settings' , array(
		'title'      => __('Settings', 'health'),
		'panel'  => 'testimonial_panel',
		'priority'   => 1,
		) );
			
			// enable service section
			$wp_customize->add_setting('hc_pro_options[testimonial_enable]',array(
			'default' => false,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'type' => 'option'
			) );
			
			$wp_customize->add_control('hc_pro_options[testimonial_enable]',array(
			'label' => __('Hide section','health'),
			'section' => 'testimonial_settings',
			'type' => 'checkbox',
			) );
			
}
add_action( 'customize_register', 'customizer_testimonial_panel' );
?>