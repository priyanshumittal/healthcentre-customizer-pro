<?php
function health_post_slug_customizer( $wp_customize ) {
//Post type slug setting
$wp_customize->add_section(
        'post_slug_setting',
        array(
            'title' => __("SEO Friendly Url's","health"),
            'description' => '',
			'priority'   => 700,
			)
    );

//Portfolio/Project Slug
$wp_customize->add_setting(
    'hc_pro_options[hc_portfolio_slug]',
    array(
        'default' => 'healthcenter_project',
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
		)
	);	
	$wp_customize->add_control('hc_pro_options[hc_portfolio_slug]',array(
    'label'   => __('Portfolio/Project slug','health'),
    'section' => 'post_slug_setting',
	 'type' => 'text',)  );	

//Portfolio/Project Slug
$wp_customize->add_setting(
    'hc_pro_options[hc_team_slug]',
    array(
        'default' => 'healthcenter_team',
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
		)
	);	
	$wp_customize->add_control('hc_pro_options[hc_team_slug]',array(
    'label'   => __('Team slug','health'),
    'section' => 'post_slug_setting',
	 'type' => 'text',)  );
	 
	 

			//Portfolio category Slug
			$wp_customize->add_setting( 'hc_pro_options[hc_products_category_slug]' , array(
			'default' => 'portfolio_categories',
			'sanitize_callback' => 'sanitize_text_field',
			'type'=>'option'
			) );
			$wp_customize->add_control('hc_pro_options[hc_products_category_slug]' , array(
			'label'          => __('Portfolio category slug', 'health' ),
			'section'        => 'post_slug_setting',
			'type'           => 'text',
			) );
	 
	 
	 
	 class health_Customize_slug extends WP_Customize_Control {
			public function render_content() { ?>
			<h3><?php _e("After Changing the slug, please do not forget to save permalinks. Without saving, the old permalinks will not revise.","health"); ?> 
			<?php
			}
			}
			
			$wp_customize->add_setting( 'hc_pro_options[health_slug_setting]', array(
			'default'				=> false,
			'capability'			=> 'edit_theme_options',
			'sanitize_callback'	=> 'wp_filter_nohtml_kses',
			));
			$wp_customize->add_control(
			new health_Customize_slug(
			$wp_customize,
			'hc_pro_options[health_slug_setting]',
			array(
				'section'				=> 'post_slug_setting',
				'settings'				=> 'hc_pro_options[health_slug_setting]',
			)));
			
			
			class WP_slug_Customize_Control extends WP_Customize_Control {
			public $type = 'new_menu';
			/**
			* Render the control's content.
			*/
			public function render_content() {
			?>
			<a href="<?php bloginfo ( 'url' );?>/wp-admin/options-permalink.php" class="button"  target="_blank"><?php _e( 'Click here permalinks setting','health' ); ?></a>
			<?php
			}
			}

			$wp_customize->add_setting(
				'slug',
				array(
					'default' => '',
					'capability'     => 'edit_theme_options',
					'sanitize_callback' => 'sanitize_text_field',
				)	
			);
			$wp_customize->add_control( new WP_slug_Customize_Control( $wp_customize, 'slug', array(	
					'section' => 'post_slug_setting',
				))
			);
	 
	 
	 
	 }
	 add_action('customize_register','health_post_slug_customizer');