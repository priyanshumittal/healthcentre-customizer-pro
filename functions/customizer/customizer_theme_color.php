<?php
function health_theme_color_customizer( $wp_customize ) {

//Theme color
class WP_color_Customize_Control extends WP_Customize_Control {
public $type = 'new_menu';

       function render_content()
       
	   {
	   echo '<h3>'.__('Predefined Colors','health').'</h3>';
		  $name = '_customize-color-radio-' . $this->id; 
		  foreach($this->choices as $key => $value ) {
            ?>
               <label>
				<input type="radio" value="<?php echo $key; ?>" name="<?php echo esc_attr( $name ); ?>" data-customize-setting-link="<?php echo esc_attr( $this->id ); ?>" <?php if($this->value() == $key){ echo 'checked="checked"'; } ?>>
				<img <?php if($this->value() == $key){ echo 'class="color_scheem_active"'; } ?> src="<?php echo get_template_directory_uri(); ?>/images/bg-patterns/<?php echo $value; ?>" alt="<?php echo esc_attr( $value ); ?>" />
				</label>
				
            <?php
		  }
		  ?>
		  <script>
			jQuery(document).ready(function($) {
				$("#customize-control-hc_pro_options-hc_stylesheet label img").click(function(){
					$("#customize-control-hc_pro_options-hc_stylesheet label img").removeClass("color_scheem_active");
					$(this).addClass("color_scheem_active");
				});
			});
		  </script>
		  <?php
       }

}
class WP_back_Customize_Control extends WP_Customize_Control {
public $type = 'new_menu';

       function render_content()
       
	   {
	   echo '<h3>'.__('Predefined Default Background','health').'</h3>';
		  $name = '_customize-radio-' . $this->id; 
		  foreach($this->choices as $key => $value ) {
            ?>
               <label>
				<input type="radio" value="<?php echo $key; ?>" name="<?php echo esc_attr( $name ); ?>" data-customize-setting-link="<?php echo esc_attr( $this->id ); ?>" <?php if($this->value() == $key){ echo 'checked'; } ?>>
				
				<img <?php if($this->value() == $key){ echo 'class="background_active"'; } ?> src="<?php echo get_template_directory_uri(); ?>/images/bg-patterns/<?php echo esc_attr( $key ); ?>" alt="<?php echo esc_attr( $value ); ?>" />
				</label>
            <?php
		  }
		  ?>
		  <script>
			jQuery(document).ready(function($) {
				$("#customize-control-hc_pro_options-hc_back_image label img").click(function(){
					$("#customize-control-hc_pro_options-hc_back_image label img").removeClass("background_active");
					$(this).addClass("background_active");
				});
			});
		  </script>
		  <?php
       }

}
/* Header Section */
	$wp_customize->add_section( 'header_image' , array(
		'title'      => __('Theme style setting', 'health'),
		'priority'   => 200,
   	) );
	
	$wp_customize->add_setting(
    'hc_pro_options[layout_selector]',
    array(
        'default' => __('wide','health'),
		'type' => 'option',
		'sanitize_callback' => 'sanitize_text_field',
		
    )
	);

	$wp_customize->add_control(
    'hc_pro_options[layout_selector]',
    array(
        'type' => 'select',
        'label' => __('Theme Layout','health'),
        'section' => 'header_image',
		'choices' => array('wide'=>'wide', 'boxed'=>'boxed'),
	));
	
	
	$wp_customize->add_setting(
	'hc_pro_options[hc_stylesheet]', array(
        'default'        => 'default.css',
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
    ));
    
	$wp_customize->add_control(new WP_color_Customize_Control($wp_customize,'hc_pro_options[hc_stylesheet]',
	array(
        'label'   => __('Predefined Colors', 'health'),
        'section' => 'header_image',
		'type' => 'radio',
		'choices' => array(
			'default.css' => 'default.png',
            'red.css' => 'red.png',
            'green.css' => 'green.png',
			'pink.css'=>'pink.png',
			'blue.css' => 'blue.png',
			'orange.css'=>'orange.png',
			'cofy.css' => 'cofy.png',
			'golden.css' => 'golden.png'
    )
	
	)));
	
	
	$wp_customize->add_setting(
    'hc_pro_options[link_color_enable]',
    array(
        'default' => false,
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
    )	
	);
	$wp_customize->add_control(
    'hc_pro_options[link_color_enable]',
    array(
        'label' => __('Skin Color Enable','health'),
        'section' => 'header_image',
        'type' => 'checkbox',
    )
	);
	
	
	$wp_customize->add_setting(
	'hc_pro_options[link_color]', array(
	'capability'     => 'edit_theme_options',
	'default' => '#31A3DD',
	'type' => 'option',
    ));
	
	$wp_customize->add_control( 
	new WP_Customize_Color_Control( 
	$wp_customize, 
	'hc_pro_options[link_color]', 
	array(
		'label'      => __( 'Skin Color', 'health' ),
		'section'    => 'header_image',
		'settings'   => 'hc_pro_options[link_color]',
	) ) );
	
	$wp_customize->add_setting(
	'hc_pro_options[hc_back_image]', array(
	'default' => 'none.png',  
	'capability'     => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
	'type' => 'option',
    ));
	$wp_customize->add_control(new WP_back_Customize_Control($wp_customize,'hc_pro_options[hc_back_image]',
	array(
        'label'   => __('Predefined Default Background', 'health'),
        'section' => 'header_image',
		'priority'   => 160,
		'type' => 'radio',
		'choices' => array(
			'bg_img1.png' => __('Pattern 0', 'health'),
            'bg_img2.png' => __('Pattern 1', 'health'),
            'bg_img3.png' => __('Pattern 2', 'health'),
            'bg_img4.png' => __('Pattern 3', 'health'),
			'bg_img5.png' => __('Pattern 4', 'health'),
			'bg_img6.png' => __('Pattern 5', 'health'),
			'bg_img7.png' => __('Pattern 6', 'health'),
			'bg_img8.png' => __('Pattern 7', 'health'),
			'none.png' => __('Pattern 8', 'health')
    )
	
	)));	
	}
	add_action( 'customize_register', 'health_theme_color_customizer' );
	?>