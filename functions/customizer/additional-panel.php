<?php
// customizer serive panel
function customizer_additional_panel( $wp_customize ) {

	//Service panel
	$wp_customize->add_panel( 'additional_panel' , array(
	'title'      => __('Homepage additional section', 'health'),
	'capability'     => 'edit_theme_options',
	'priority'   => 560,
   	) );
	
		//Service panel
		$wp_customize->add_section( 'additional_settings' , array(
		'title'      => __('Settings', 'health'),
		'panel'  => 'additional_panel',
		'priority'   => 1,
		) );
			
			// enable service section
			$wp_customize->add_setting('hc_pro_options[additional_enable]',array(
			'default' => false,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'type' => 'option'
			) );
			
			$wp_customize->add_control('hc_pro_options[additional_enable]',array(
			'label' => __('Hide section','health'),
			'section' => 'additional_settings',
			'type' => 'checkbox',
			) );
			
			// Number of services
			$wp_customize->add_setting('hc_pro_options[additional_column_layout]',array(
			'default' => 4,
			'type' => 'option',
			'sanitize_callback' => 'sanitize_text_field',
			) );

			$wp_customize->add_control('hc_pro_options[additional_column_layout]',array(
			'type' => 'select',
			'label' => __('Select column layout','health'),
			'section' => 'additional_settings',
			'choices' => array(2=>'2',3=>'3',4=>'4'),
			) );
			
			
			// headings
		$wp_customize->add_section( 'additional_headings' , array(
		'title'      => __('Section Header', 'health'),
		'panel'  => 'additional_panel',
		'priority'   => 2,
		) );
			
			// Additional title
			$wp_customize->add_setting('hc_pro_options[additional_title]',array(
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'type' => 'option'
			) );
			
			$wp_customize->add_control('hc_pro_options[additional_title]',array(
			'label' => __('Title','health'),
			'section' => 'additional_headings',
			'type' => 'text',
			) );
			
			// service description
			$wp_customize->add_setting('hc_pro_options[additional_description]',array(
			'sanitize_callback' => 'sanitize_text_field',
			'type' => 'option'
			) );
			
			$wp_customize->add_control('hc_pro_options[additional_description]',array(
			'label' => __('Subtitle','health'),
			'section' => 'additional_headings',
			'type' => 'textarea',
			'sanitize_callback' => 'sanitize_text_field',
			) );
			
}
add_action( 'customize_register', 'customizer_additional_panel' );