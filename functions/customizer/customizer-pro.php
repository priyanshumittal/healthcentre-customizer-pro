<?php
//Pro Button

function health_pro_customizer( $wp_customize ) {
class WP_Pro_Customize_Control extends WP_Customize_Control {
    public $type = 'new_menu';
    /**
    * Render the control's content.
    */
    public function render_content() {
    ?>
     <div class="pro-box">
<a href="<?php echo esc_url('http://www.webriti.com/support/categories/healthcentre');?>" target="_blank" class="upgrade" id="review_pro"><?php _e( 'Support forum','health' ); ?></a>
		
	</div>
    <?php
    }
}
$wp_customize->add_section( 'health_pro_section' , array(
		'title'      => __('Theme support', 'health'),
		'priority'   => 1400,
   	) );

$wp_customize->add_setting(
    'upgrade_pro',
    array(
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
    )	
);
$wp_customize->add_control( new WP_Pro_Customize_Control( $wp_customize, 'upgrade_pro', array(
		'section' => 'health_pro_section',
		'setting' => 'upgrade_pro',
    ))
);

class WP_document_Customize_Control extends WP_Customize_Control {
    public $type = 'new_menu';
    /**
    * Render the control's content.
    */
    public function render_content() {
    ?>
      <div class="pro-box">
<a href="<?php echo esc_url('http://webriti.com/help/category/themes/health-center/');?>" target="_blank" class="document" id="review_pro"><?php _e( 'Help articles','health' ); ?></a>
	 <div>
    <?php
    }
}

$wp_customize->add_setting(
    'doc_Review',
    array(
        'default' => '',
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
    )	
);
$wp_customize->add_control( new WP_document_Customize_Control( $wp_customize, 'doc_Review', array(	
		'section' => 'health_pro_section',
		'setting' => 'doc_Review',
    ))
);

}
add_action( 'customize_register', 'health_pro_customizer' );
?>