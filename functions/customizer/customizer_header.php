<?php
function health_header_customizer( $wp_customize ) {
/* Header Section */
	$wp_customize->add_panel( 'header_options', array(
		'priority'       => 300,
		'capability'     => 'edit_theme_options',
		'title'      => __('Header settings', 'health'),
	) );
	
	//Custom css
	$wp_customize->add_section( 'custom_css' , array(
		'title'      => __('Custom CSS', 'health'),
		'panel'  => 'header_options',
		'priority'   => 100,
   	) );
	$wp_customize->add_setting(
	'hc_pro_options[webrit_custom_css]'
		, array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type'=> 'option',
    ));
    $wp_customize->add_control( 'hc_pro_options[webrit_custom_css]', array(
        'label'   => __('Custom CSS', 'health'),
        'section' => 'custom_css',
        'type' => 'textarea',
    )); 

	$wp_customize->add_section(
        'footer_google_analytics',
        array(
            'title' => __('Google analytics setting','health'),
            'priority'    => 500,
			'panel' => 'header_options',
        )
    );
	
	// Google Anlytics
	$wp_customize->add_setting(
    'hc_pro_options[google_analytics]',
    array(
        'default' => '',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
    )
	
	);
	$wp_customize->add_control(
    'hc_pro_options[google_analytics]',
    array(
        'label' => __('Google Analytics','health'),
        'section' => 'footer_google_analytics',
        'type' => 'textarea',
    )
	);
	
	}
	add_action( 'customize_register', 'health_header_customizer' );
	?>