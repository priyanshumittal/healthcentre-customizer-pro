<?php
function health_portfolio_page_customizer( $wp_customize ) {

/* Header Section */
	$wp_customize->add_section( 'project_section_settings', array(
		'capability'     => 'edit_theme_options',
		'priority'   => 1300,
		'title'      => __('Portfolio category page setting', 'health'),
	) );

$wp_customize->add_setting(
    'hc_pro_options[taxonomy_portfolio_list]',
    array(
       'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
		'type' => 'option',
		'default' => 2,
		)
	);	
	$wp_customize->add_control( 'hc_pro_options[taxonomy_portfolio_list]',array(
	 'type' => 'select',
	 'label'   => __('Select column layout','health'),
    'section' => 'project_section_settings',
	 'choices' => array(2=>'2',3=>'3',4=>'4'),
		)
	);
	
	//link
	class WP_project_Customize_Control extends WP_Customize_Control {
		public $type = 'new_menu';
		/**
		* Render the control's content.
		*/
		public function render_content() {
		?>
		<a href="<?php bloginfo ( 'url' );?>/wp-admin/edit.php?post_type=healthcenter_project" class="button"  target="_blank"><?php _e( 'Click here to add project', 'health' ); ?></a>
		<?php
		}
	}

	$wp_customize->add_setting(
		'project',
		array(
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
		)	
	);
	$wp_customize->add_control( new WP_project_Customize_Control( $wp_customize, 'project', array(	
			'section' => 'project_section_settings',
		))
	);	


}
add_action( 'customize_register', 'health_portfolio_page_customizer' );