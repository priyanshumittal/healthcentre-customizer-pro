<?php
function health_template_customizer( $wp_customize ) {

//Template panel 
	$wp_customize->add_panel( 'health_template', array(
		'priority'       => 800,
		'capability'     => 'edit_theme_options',
		'title'      => __('Template settings', 'health'),
	) );
	
	
// add section to manage About us tPage
	$wp_customize->add_section(
        'about_us_setting',
        array(
            'title' => __('About us page setting','health'),
			'panel'  => 'health_template',
			'priority'   => 100,
			
			)
    );
// About us page Temm heading one
	$wp_customize->add_setting(
		'hc_pro_options[hc_head_one_team]',
		array('capability'  => 'edit_theme_options',
		'default' => __('Meet Our','health'), 
		'type' => 'option',
		));

	$wp_customize->add_control(
		'hc_pro_options[hc_head_one_team]',
		array(
			'type' => 'text',
			'label' => __('Heading one','health'),
			'section' => 'about_us_setting',
		)
	);
	
//About Us Page Team Heading Two
	$wp_customize->add_setting(
		'hc_pro_options[hc_head_two_team]',
		array('capability'  => 'edit_theme_options',
		'default' => __('Great Team','health'), 
		'type' => 'option',
		));

	$wp_customize->add_control(
		'hc_pro_options[hc_head_two_team]',
		array(
			'type' => 'text',
			'label' => __('Heading two','health'),
			'section' => 'about_us_setting',
		)
	);
	
// About Us Page Team Tag line
	$wp_customize->add_setting(
		'hc_pro_options[hc_head_team_tagline]',
		array('capability'  => 'edit_theme_options',
		'default' => __('We provide best WordPress solutions for your business. Thanks to our framework you will get more happy customers.','health'), 
		'type' => 'option',
		));

	$wp_customize->add_control(
		'hc_pro_options[hc_head_team_tagline]',
		array(
			'type' => 'textarea',
			'label' => __('Description','health'),
			'section' => 'about_us_setting',
		)
	);
	
	
//Add Team setting
	class WP_team_Customize_Control extends WP_Customize_Control {
    public $type = 'new_menu';
    /**
    * Render the control's content.
    */
    public function render_content() {
    ?>
    <a href="<?php bloginfo ( 'url' );?>/wp-admin/edit.php?post_type=healthcenter_team" class="button"  target="_blank"><?php _e( 'Click here to team member', 'health' ); ?></a>
    <?php
    }
}

$wp_customize->add_setting(
    'team',
    array(
        'default' => '',
		'capability'     => 'edit_theme_options',
		'sanitize_callback' => 'sanitize_text_field',
    )	
);
$wp_customize->add_control( new WP_team_Customize_Control( $wp_customize, 'team', array(	
		'section' => 'about_us_setting',
		'priority'   => 500,
    ))
);	
	

	
	// Conatct Google map
	$wp_customize->add_section(
        'contact_page_map',
        array(
            'title' => __('Contact page setting','health'),
			'panel'  => 'health_template',
			'priority'   => 100,
			
			)
    );
	
	
	// Contact Office time:
	$wp_customize->add_setting(
		'hc_pro_options[contact_google_map_enabled]',
		array('capability'  => 'edit_theme_options',
		'default' => true, 
		'type' => 'option',
		));

	$wp_customize->add_control(
		'hc_pro_options[contact_google_map_enabled]',
		array(
			'type' => 'checkbox',
			'label' => __('Enable Google Map in contact page','health'),
			'section' => 'contact_page_map',
		)
	);
	
	//Google map URL
	
	$wp_customize->add_setting(
		'hc_pro_options[hc_contact_google_map_shortcode]',
		array('capability'  => 'edit_theme_options',
		'default' => '', 
		'type' => 'option',
		));

	$wp_customize->add_control(
		'hc_pro_options[hc_contact_google_map_shortcode]',
		array(
			'type' => 'textarea',
			'label' => __('Google Map Shortcode','health'),
			'section' => 'contact_page_map',
		)
	);
	
	}
	add_action( 'customize_register', 'health_template_customizer' );
	?>