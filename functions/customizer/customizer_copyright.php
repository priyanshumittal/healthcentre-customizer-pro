<?php
// Footer copyright section 
function health_copyright_customizer( $wp_customize ) {
	
	$wp_customize->add_panel('copyright_panel',array(
    'title' => __('Footer copyright settings','health'),
	'capability'     => 'edit_theme_options',
    'priority' => 580,
    ) );
	
		$wp_customize->add_section('copyright_section',array(
			'title' => __('Footer copyright settings','health'),
			'panel'=>'copyright_panel',
			'priority' => 1,
		));
	
			$wp_customize->add_setting(
			'hc_pro_options[footer_copyright]',
			array(
				'default' => '',
				'type' =>'option'
			));
			
			$wp_customize->add_control(
			'hc_pro_options[footer_copyright]',
			array(
				'label' => __('Copyright text','health'),
				'section' => 'copyright_section',
				'type' => 'textarea',
			));		
}
add_action( 'customize_register', 'health_copyright_customizer' );