<?php
// customizer serive panel
function customizer_faq_panel( $wp_customize ) {

	//Service panel
	$wp_customize->add_panel( 'faq_panel' , array(
	'title'      => __('FAQ section', 'health'),
	'capability'     => 'edit_theme_options',
	'priority'   => 550,
   	) );
	
		//Service panel
		$wp_customize->add_section( 'faq_settings' , array(
		'title'      => __('Settings', 'health'),
		'panel'  => 'faq_panel',
		'priority'   => 1,
		) );
			
			// enable service section
			$wp_customize->add_setting('hc_pro_options[faq_enable]',array(
			'default' => false,
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'type' => 'option'
			) );
			
			$wp_customize->add_control('hc_pro_options[faq_enable]',array(
			'label' => __('Hide section','health'),
			'section' => 'faq_settings',
			'type' => 'checkbox',
			) );
			
		// headings
		$wp_customize->add_section( 'faq_headings' , array(
		'title'      => __('Section Header', 'health'),
		'panel'  => 'faq_panel',
		'priority'   => 2,
		) );
			
			// Service title
			$wp_customize->add_setting('hc_pro_options[hc_head_faq]',array(
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'type' => 'option'
			) );
			
			$wp_customize->add_control('hc_pro_options[hc_head_faq]',array(
			'label' => __('Title','health'),
			'section' => 'faq_headings',
			'type' => 'text',
			) );
	
}
add_action( 'customize_register', 'customizer_faq_panel' );