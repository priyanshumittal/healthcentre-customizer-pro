<?php
 if ( function_exists( 'add_image_size' ) ) 
 { 
	//testimonial
	add_image_size('health_center_testimonial',50,50,true);
	add_image_size('crop_hc_media_sidebar_img',60,60,true);
}
// code for home slider post types 
add_filter( 'intermediate_image_sizes', 'hc_image_presets');

function hc_image_presets($sizes){
   $type = get_post_type($_REQUEST['post_id']);	
    foreach($sizes as $key => $value){
    	if($type=='health_testimonial'  &&  $value != 'health_center_testimonial' )
		 {        unset($sizes[$key]);      }
		else if($type=='post'  &&  $value != 'health_center_recentpostwidget' &&  $value != 'full' && $value !='crop_hc_media_sidebar_img'  && $value !='health_blog_fullwidth')
		 {        unset($sizes[$key]);      }			
    }
    return $sizes;	 
}
?>