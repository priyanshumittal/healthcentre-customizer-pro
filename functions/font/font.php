<?php

/*--------------------------------------------------------------------*/
/*     Register Google Fonts
/*--------------------------------------------------------------------*/
function health_fonts_url() {
	
	$hc_pro_options=theme_data_setup();
	$current_options = wp_parse_args(  get_option( 'hc_pro_options', array() ), $hc_pro_options );
	
    $fonts_url = '';
		
    $font_families = array();
	
	$font_families[] = 'Open Sans:300,400,600,700,800';
	$font_families[] = 'Roboto:100,300,400,500,700,900';
	$font_families[] = 'Raleway:400,600,700';
	$font_families[] = 'italic';
	
	if(!in_array( $current_options['general_typography_fontfamily'] , $font_families ))
    {
		$font_families[] = $current_options['general_typography_fontfamily'].':100,300,400,400italic,700';
    }
	
	if(!in_array( $current_options['menu_title_fontfamily'] , $font_families ))
    {
		$font_families[] = $current_options['menu_title_fontfamily'].':100,300,400,400italic,700';
    }
	
	if(!in_array( $current_options['post_title_fontfamily'] , $font_families ))
    {
		$font_families[] = $current_options['post_title_fontfamily'].':100,300,400,400italic,700';
    }
	
	if(!in_array( $current_options['service_title_fontfamily'] , $font_families ))
    {
		$font_families[] = $current_options['service_title_fontfamily'].':100,300,400,400italic,700';
    }
	
	if(!in_array( $current_options['portfolio_title_fontfamily'] , $font_families ))
    {
		$font_families[] = $current_options['portfolio_title_fontfamily'].':100,300,400,400italic,700';
    }
	
	if(!in_array( $current_options['widget_title_fontfamily'] , $font_families ))
    {
		$font_families[] = $current_options['widget_title_fontfamily'].':100,300,400,400italic,700';
    }
	
	if(!in_array( $current_options['calloutarea_title_fontfamily'] , $font_families ))
    {
		$font_families[] = $current_options['calloutarea_title_fontfamily'].':100,300,400,400italic,700';
    }
	
	if(!in_array( $current_options['calloutarea_description_fontfamily'] , $font_families ))
    {
		$font_families[] = $current_options['calloutarea_description_fontfamily'].':100,300,400,400italic,700';
    }
	
	if(!in_array( $current_options['calloutarea_purches_fontfamily'] , $font_families ))
    {
		$font_families[] = $current_options['calloutarea_purches_fontfamily'].':100,300,400,400italic,700';
    }
 
        $query_args = array(
            'family' => urlencode( implode( '|', $font_families ) ),
            'subset' => urlencode( 'latin,latin-ext' ),
        );
 
        $fonts_url = add_query_arg( $query_args, '//fonts.googleapis.com/css' );

    return $fonts_url;
}
function health_scripts_styles() {
    wp_enqueue_style( 'health-fonts', health_fonts_url(), array(), null );
}
add_action( 'wp_enqueue_scripts', 'health_scripts_styles' );
?>