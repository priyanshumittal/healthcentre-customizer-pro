<?php
// Template Name: Contact Page With CF7

get_header(); ?>
	
<!-- HC Contact Form Section -->
<?php $current_options = wp_parse_args( get_option('hc_pro_options', array() ), theme_data_setup());
	$mapsrc= $current_options['hc_contact_google_map_url']; 
	$mapsrc=$mapsrc.'&amp;output=embed';
?>
<div class="container">
	<div class="row">
		<div class="hc_page_header_area">
			<h1><?php the_title(); ?></h1>							
		</div>
	</div>
</div>
	
<!-- HC Google Map Section -->	
<?php if($current_options['contact_google_map_enabled'] == true){?>
<div class="container">
	<div class="row hc_contactv1_section">
		<div class="col-md-12 hc_google_map">			
			<?php if( $current_options['hc_contact_google_map_shortcode'] !='' ) {
				
				echo  do_shortcode($current_options['hc_contact_google_map_shortcode']); 
			}
			else
			{
				$current_options = get_option('hc_pro_options');
				if(!empty($current_options)) { ?>
				<iframe width="100%" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="<?php echo $mapsrc; ?>"></iframe>
			<?php } 
			} ?>
		</div>
	</div>	
</div>
<?php } ?>
<!-- /HC Google Map Section -->
<div class="container">
	<div class="row">	
		<div class="col-md-8 hc_contactv1_area">
			<div id="myformdata">
				<?php
				$current_options = wp_parse_args( get_option('hc_pro_options', array() ), theme_data_setup());
				if($current_options['hc_send_usmessage']!='') { ?>
					<h2><?php echo $current_options['hc_send_usmessage']; ?></h2>
				<?php } ?>
				
				<?php 
					the_post();
					the_content(); 
				?>
				
			</div>
		</div>
		
		<?php if( is_active_sidebar('sidebar-contact') ) : ?>
		<div class="col-md-4 hc_contactv1_sidebar">
			<?php dynamic_sidebar('sidebar-contact'); ?>
		</div>
		<?php else : ?>
			<div class="col-md-4 hc_contactv1_sidebar">
				<div class="hc_contactv1_address">
					<?php if($current_options['hc_get_in_touch']!='') { ?>
					<h3><?php echo $current_options['hc_get_in_touch']; ?></h3>
					<?php } ?>
					<address>
					  <?php if($current_options['hc_contact_address']!='')
					  { echo $current_options['hc_contact_address']; } ?><br>
					  <?php if($current_options['hc_contact_address_two']!='')
					  { echo $current_options['hc_contact_address_two']; } ?><br>
					  <?php if($current_options['hc_contact_phone_number']!='') { ?>
					  <abbr title="<?php _e('Phone','health'); ?>"><?php _e('Phone','health'); ?>:</abbr><?php echo $current_options['hc_contact_phone_number']; ?><br>
					  <?php } ?>
					  <?php if($current_options['hc_contact_fax_number']!='') { ?>
					  <abbr title="<?php _e('Fax','health'); ?>"><?php _e('Fax','health'); ?>:</abbr><?php echo $current_options['hc_contact_fax_number']; ?><br>
					  <?php } ?>
					  <?php if($current_options['hc_contact_email']!='') { ?>
					  <abbr title="<?php _e('Email','health')?>"><?php _e('Email','health')?>:</abbr> <a href="mailto:#"><?php echo $current_options['hc_contact_email']; ?></a><br>
					  <?php } ?>
					</address>
				</div>
				<?php if($current_options['social_media_in_contact_page_enabled'] ==true) { ?>
				<div class="hc_contactv1_address">
					<h3><?php _e('Social Network','health'); ?></h3>
					<div class="hc_contactv1_social">
					<?php if($current_options['social_media_facebook_link']!='') { ?>
						<a href="<?php echo $current_options['social_media_facebook_link']; ?>" class="facebook">&nbsp;</a>
					<?php }
						if($current_options['social_media_twitter_link']!='') { ?>
						<a href="<?php echo $current_options['social_media_twitter_link']; ?>" class="twitter">&nbsp;</a>
					<?php }
						if($current_options['social_media_linkedin_link']!='') { ?>
						<a href="<?php echo $current_options['social_media_linkedin_link']; ?>" class="linked_in">&nbsp;</a>
					<?php }
						if($current_options['social_media_google_plus']!='') { ?>
						<a href="<?php echo $current_options['social_media_google_plus']; ?>"  class="google_plus">&nbsp;</a>
					<?php } ?>
					</div>
				</div>	
				<?php } ?>
			</div>
		<?php endif; ?>
		
	</div>	
</div>
<!-- /HC Contact Form Section -->
<?php get_footer(); ?>