<?php
$current_options = wp_parse_args( get_option('hc_pro_options', array() ), theme_data_setup());
if($current_options['testimonial_enable'] == false) {
 
// testimonial section
if( is_active_sidebar('sidebar-testimonial') ){
	dynamic_sidebar('sidebar-testimonial');
}
else
	{
		$current_options = get_option('hc_pro_options');
		if(!empty($current_options)) {
			get_template_part('index','testimonials_two');
		}		
	}
 }