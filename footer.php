<?php $current_options = wp_parse_args( get_option('hc_pro_options', array() ), theme_data_setup()); ?>
<!-- Footer Widget Secton -->
<div class="hc_footer_widget_area">	
	<div class="container">
		<div id="sidebar-footer" class="row sidebar-footer">
		<?php if ( is_active_sidebar( 'footer-widget-area' ) )
		{ 
			dynamic_sidebar( 'footer-widget-area' ); 
		} 
		?>
		</div>
		
		<div class="row hc_footer_area">
		
			<div class="col-md-8">
				<?php 
				if($current_options['footer_copyright']){
					echo $current_options['footer_copyright'];
				}
			    else { if(!empty($current_options)) {
				?>
				<p>
					<?php  
					if($current_options['footer_customizations']!='') { echo $current_options['footer_customizations']; }	?>
					<a href="http://wordpress.org/"><?php if($current_options['powered_by_text']!='') { echo $current_options['powered_by_text']; } ?></a>.<?php if($current_options['created_by_text']!='') { echo $current_options['created_by_text']; } ?>
					<a href="<?php if($current_options['created_by_link']!='') { echo $current_options['created_by_link']; } ?>"><?php if($current_options['created_by_webriti_text']!='') { echo $current_options['created_by_webriti_text']; } ?></a>
				</p>
				<?php } } ?>
			</div>
			
			<?php if( is_active_sidebar('footer-right-section') ) { ?>
			<div class="col-md-4 sidebar-footer-right">
				<?php dynamic_sidebar('footer-right-section'); ?>
			</div>
			<?php } 
			else
			{
				if(!empty($current_options))
				{ ?>
				  <?php if($current_options['footer_social_media_enabled']==true) { ?>
						<div class="col-md-4">
							<div class="hc_footer_social">
								<?php if($current_options['social_media_facebook_link']!='') { ?>
								<a class="facebook" id="fb_tooltip" href="<?php echo $current_options['social_media_facebook_link']; ?>">&nbsp;</a>
								<?php }
								if($current_options['social_media_twitter_link']!='') { ?>
								<a class="twitter" id="twi_tooltip" href="<?php echo $current_options['social_media_twitter_link']; ?>">&nbsp;</a>
								<?php }
								if($current_options['social_media_linkedin_link']!='') { ?>
								<a class="linked-in" id="in_tooltip" href="<?php echo $current_options['social_media_linkedin_link']; ?>">&nbsp;</a>
								<?php }
								if($current_options['social_media_google_plus']!='') { ?>
								<a class="google_plus" id="plus_tooltip" href="<?php echo $current_options['social_media_google_plus']; ?>">&nbsp;</a>
								<?php } ?>
							</div>
						</div>
				<?php } 
				}
				
			}
			?>
			
		</div>
	</div>
</div>
</div>
<a href="#" class="hc_scrollup"><i class="fa fa-chevron-up"></i></a>
<?php wp_footer(); ?>
</body>
</html>