<?php $hc_current_options = wp_parse_args( get_option('hc_pro_options', array() ), theme_data_setup()); ?>
<div class="head_cont_info sidebar-header">
	<ul class="header-icon"><?php if($hc_current_options['hc_contact_email']!='') { ?>
		<li><i class="fa fa-envelope"></i><a href="mailto:<?php echo $hc_current_options['hc_contact_email']; ?>"><?php echo $hc_current_options['hc_contact_email']; ?></a></li>
		<?php } 
		if($hc_current_options['hc_contact_phone_number']!='') {
		?>
		<li><i class="fa fa-mobile"></i>+<?php echo $hc_current_options['hc_contact_phone_number']; ?></li>
		<?php } ?>
	</ul>
</div>
<div class="clear"></div>
<?php if($hc_current_options['header_social_media_enabled']==true) {?>
<ul class="head_social_icons">
	<?php if($hc_current_options['social_media_facebook_link']!='') { ?>
	<li class="facebook"><a href="<?php echo $hc_current_options['social_media_facebook_link']; ?>"></a></li>
	<?php }
	if($hc_current_options['social_media_twitter_link']!='') { ?>
	<li class="twitter"><a href="<?php echo $hc_current_options['social_media_twitter_link']; ?>"></a></li>
	<?php }
	if($hc_current_options['social_media_google_plus']!='') { ?>
	<li class="google"><a href="<?php echo $hc_current_options['social_media_google_plus']; ?>"></a></li>
	<?php }
	if($hc_current_options['social_media_linkedin_link']!='') { ?>
	<li class="linkedin"><a href="<?php echo $hc_current_options['social_media_linkedin_link']; ?>"></a></li>
	<?php } ?>					
</ul>
<?php } ?>