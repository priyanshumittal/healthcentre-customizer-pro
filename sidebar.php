<?php
/*	@Theme Name	:	Health-Center
* 	@file         :	sidebar.php
* 	@package      :	Health-Center
* 	@author       :	VibhorPurandare
* 	@license      :	license.txt
* 	@filesource   :	wp-content/themes/health-center/sidebar.php
*/	
?>

<?php if( is_active_sidebar('sidebar-primary') ): ?>
<div class="col-md-4 secondory">	
   <?php
		dynamic_sidebar('sidebar-primary');
	?>
</div>
<?php endif; ?>