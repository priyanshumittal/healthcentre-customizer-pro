<!-- Slider -->
<?php
$current_options = wp_parse_args( get_option('hc_pro_options', array() ), theme_data_setup());
$animation= $current_options['animation'];
$animationSpeed=$current_options['animationSpeed'];
$direction=$current_options['slide_direction'];
$slideshowSpeed=$current_options['slideshowSpeed'];
$home_slider_enabled = $current_options['home_slider_enabled'];
?>
<script>
jQuery(window).load(function() {
  // The slider being synced must be initialized first
  jQuery('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
	directionNav: false,
    animationLoop: false,
    slideshow: false,
    asNavFor: '#slider'
  });
   
  jQuery('#slider').flexslider({
	animation: "<?php echo $animation; ?>",
	animationSpeed: <?php echo $animationSpeed; ?>,
	direction: "<?php echo $direction; ?>",
	slideshowSpeed: <?php echo $slideshowSpeed; ?>,
    useCSS: false,
  });
});
</script>
<?php if($home_slider_enabled == true) {  
if($current_options['slider_category'] !='')
{
	$arg = array( 'post_type'=>'post','category__in' => $current_options['slider_category'],'ignore_sticky_posts' => 1);
	$loop = new WP_Query( $arg );
	if ( $loop->have_posts() ) :
	?>
	<div class="hc_slider">
	<div id="slider" class="flexslider">
		<ul class="slides">	
		<?php 
		while ( $loop->have_posts() ) : $loop->the_post(); 
			
			$btn_enable = sanitize_text_field( get_post_meta( get_the_ID(), 'btn_enable', true ));
			$btn_text = sanitize_text_field( get_post_meta( get_the_ID(), 'btn_text', true ));
			$btn_link = sanitize_text_field( get_post_meta( get_the_ID(), 'btn_link', true ));
			$btn_target = sanitize_text_field( get_post_meta( get_the_ID(), 'btn_target', true ));
			
			if(has_post_thumbnail()):
		?>
			<li>
				<?php $defalt_arg =array('class' => "img-responsive"); ?>
				<?php the_post_thumbnail('home_slider', $defalt_arg); ?>	
				
				<div class="slide-caption">
					<div class="slide-text-bg1"><h1><?php the_title(); ?></h1></div>
					
					<?php if( has_excerpt() ): ?>
					<div class="slide-text-bg2">
						<span><?php the_excerpt(); ?></span>
					</div>
					<?php endif; ?>
					
					<?php if( $btn_enable ): ?>
					<div class="slide-btn-area-sm">
						<a href="<?php echo $btn_link; ?>" class="slide-btn-sm" <?php if( $btn_target) echo 'target="_blank"'; ?>><?php if($btn_text)echo $btn_text; else echo __('Read More','health'); ?></a>
					</div>
					<?php endif; ?>
					
				</div>
				<?php  ?>
			</li>
		<?php 
			endif;
		endwhile; 
		?>
		</ul>
	</div>
	<?php endif; echo '</div>'; } else { 
		$current_options = get_option('hc_pro_options');
		if(!empty($current_options)) {
		get_template_part('index','slidertwoold');
} } }
	 ?>
<!-- /Slider -->