<?php
/*	@Theme Name	:	Health-Center
* 	@file         :	page.php
* 	@package      :	Health-Center
* 	@author       :	VibhorPurandare
* 	@license      :	license.txt
* 	@filesource   :	wp-content/themes/health-center/page.php
*/
?>
<?php get_header(); ?>
<div class="container">
	<div class="row">
		<div class="hc_page_header_area">
			<h1><?php the_title(); ?></h1>				
		</div>
	</div>
</div>
<!-- /HC Page Header Section -->

<!-- /HC Page Header Section -->
<!-- HC Blog right Sidebar Section -->	
<div class="container">
	<div class="row hc_blog_wrapper">
		<?php $content_page_layout = get_post_meta( get_the_ID(),'content_page_layout', true ); ?>
		<?php if($content_page_layout == "fullwidth_left") {  get_sidebar();  } ?>
		<?php if($content_page_layout == "fullwidth") { $page_width_type=12; } else { $page_width_type = 8; } ?>
		<!--Blog Content-->
		<div class="col-md-<?php echo $page_width_type; ?>">
			<div class="hc_blog_detail_section">					
				<div class="clear"></div>
				<?php $defalt_arg =array('class' => "img-responsive" ); ?>
				<?php if(has_post_thumbnail()): ?>
				<div class="hc_blog_post_img">					
					<a  href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail('full', $defalt_arg); ?>
					</a>	
				</div>
				<?php endif; ?>	
				<div class="hc_blog_post_content"><?php the_post(); the_content( __( 'Read More' , 'health' ) ); ?></div>	
			</div>
			<?php comments_template('',true); ?>
		</div>
		<?php if($content_page_layout == "fullwidth_right") {  get_sidebar(); } ?>
	</div>
</div>
<?php get_footer(); ?>