<?php	
		if('page' == get_option('show_on_front')){ get_template_part('index');}
		
		else
		{
		$hc_pro_options=theme_data_setup();
		$hc_pro_options = wp_parse_args(  get_option( 'hc_pro_options', array() ), $hc_pro_options );
		get_header();
		get_template_part('index','slider');
		$data =is_array($hc_pro_options['front_page_data']) ? $hc_pro_options['front_page_data'] : explode(",",$hc_pro_options['front_page_data']);
		if($data) 
		{
			foreach($data as $key=>$value)
			{			
				switch($value) 
				{	
					case 'Service': 
					//****** get index service  ********
					get_template_part('index', 'service') ;
					break;
					
					case 'Project':
					//****** get index project  ********
					get_template_part('index', 'project');					
					break;
					
					case 'News': 			
					//****** get index recent blog  ********
					get_template_part('index', 'news');					
					break; 	
					
					case 'Testimonials': 			
					//****** get index recent blog  ********
					get_template_part('index', 'testimonials');					
					break;
					
					case 'Additional': 			
					//****** get index Additional Section  ********
					get_template_part('index', 'additional');					
					break;
					
					case 'CallOut': 			
					//****** get index recent blog  ********
					get_template_part('index', 'calloutarea');					
					break; 
				}
			}
			
		} 	
	get_footer(); 
	}
?>