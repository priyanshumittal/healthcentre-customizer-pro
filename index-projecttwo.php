<?php  
		 $current_options = wp_parse_args( get_option('hc_pro_options', array() ), theme_data_setup());
		 $j=1;
	     $total_project = $current_options['project_list']; 
		
		$args = array( 'post_type' => 'healthcenter_project','posts_per_page' =>$total_project); 	
		$project = new WP_Query( $args ); 
		$hc_img_responsive=array('class'=>'hc_img_responsive');
		if( $project->have_posts() )
		{ while ( $project->have_posts() ) : $project->the_post(); ?>
		<?php 
		    $link=1;
			if(get_post_meta( get_the_ID(),'meta_project_link', true )) 
			{ $meta_project_link=get_post_meta( get_the_ID(),'meta_project_link', true ); }
			else { 	    $link=0; } ?>
					
		<div class="col-md-3 hc_home_portfolio_area">
			<div class="hc_home_portfolio_showcase">
				<div class="hc_home_portfolio_showcase_media">
					<?php if(has_post_thumbnail()):?>
						<?php the_post_thumbnail('portfolio-4c-thumb',$hc_img_responsive); 
						$post_thumbnail_id = get_post_thumbnail_id();
						$post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id );						
					?>
					<!--<img class="hc_img_responsive" alt="Health Center" src="<?php echo WEBRITI_TEMPLATE_DIR_URI; ?>/images/hc_home_port1.jpg"> -->
					<div class="hc_home_portfolio_showcase_overlay">
						<div class="hc_home_portfolio_showcase_overlay_inner">
							<div class="hc_home_portfolio_showcase_icons">
								<a title="Health Center" href="<?php echo $meta_project_link; ?>" <?php  if(get_post_meta( get_the_ID(),'meta_project_target', true )) { echo "target='_blank'"; }  ?>><i class="fa fa-link"></i></a>
								<a href="<?php echo $post_thumbnail_url; ?>"  data-lightbox="image" title="<?php the_title(); ?>" class="hover_thumb"><i class="fa fa-picture-o"></i></a>
							</div>
						</div>
					</div>
					<?php endif; ?>
				</div>
			</div>
			<div class="hc_home_portfolio_caption">
			    <?php if ($link==1 ) { ?>
				<h3><a href="<?php echo $meta_project_link; ?>" <?php  if(get_post_meta( get_the_ID(),'meta_project_target', true )) { echo "target='_blank'"; }  ?>><?php echo the_title(); ?></a></h3>
				<?php } else { ?><h3><?php echo the_title(); ?> </h3> <?php } ?>
				<small><?php echo get_post_meta( get_the_ID(),'portfolio_project_summary', true ); ?></small>	
			</div>
		</div>	
		<?php  if($j%4==0){	echo "<div class='clearfix'></div>"; } $j++; endwhile; 
		} ?>