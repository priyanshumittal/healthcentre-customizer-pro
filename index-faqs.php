<?php 
			$current_options = wp_parse_args( get_option('hc_pro_options', array() ), theme_data_setup());
			$i=1;
			
			
			if( get_option('hc_pro_options') ):
				
				foreach($current_options['hc_faq'] as $tt){ 
				
				if($tt['faq-title']!=null):
				?>
					<div class="hc_panel panel-default">
						<div class="hc_panel-heading">
						  <h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i; ?>">
							  <span class="fa fa-<?php if($i=="1"){ ?>minus<?php } else { ?>plus<?php } ?>"></span>
							  <?php echo $tt['faq-title']; ?>
							</a>
						  </h4>
						</div>
						<div id="collapse<?php echo $i; ?>" class="panel-collapse collapse <?php if($i=="1"){ ?> in <?php } ?> ">
							<div class="panel-body">
								<p><?php echo  $tt['faq-text']; ?></p>
							</div>
						</div>
					</div>					
					<?php
					  endif;
					  
						$i=$i+1;
					}
					
			endif;
			?>