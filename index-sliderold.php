<div class="hc_slider">
		<?php 	
		$count_posts = wp_count_posts( 'healthcenter_slider')->publish;
		$args = array( 'post_type' => 'healthcenter_slider','posts_per_page' =>$count_posts); 	
		$slider = new WP_Query( $args );
		if( $slider->have_posts() )
		{ ?>
			<div id="slider" class="flexslider">
			<ul class="slides">	
			<?php while ( $slider->have_posts() ) : $slider->the_post(); ?>
				<li><?php if(has_post_thumbnail()):?>
						<?php $defalt_arg =array('class' => "img-responsive"); ?>
						<?php the_post_thumbnail('home_slider', $defalt_arg); ?>
						<?php endif; ?>					
				</li>
			<?php endwhile; ?>
			</ul>
		</div>		
		<div id="carousel" class="flexslider">
			<ul class="slides slide_thumb">
				<?php $i=1; while ( $slider->have_posts() ) : $slider->the_post(); ?>
				<li  class="<?php if($i==1){ echo 'flex-active';$i=2;}?>">
					<h5><?php echo the_title(); ?></h5>
					<p><?php echo get_post_meta( get_the_ID(),'slider_text', true ) ; ?></p>
				</li>
				<?php endwhile; ?>
			</ul>
		</div>			
		<?php } ?>
</div>