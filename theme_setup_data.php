<?php
/*---------------------------------------------------------------------------------*
 * @file           theme_stup_data.php
 * @package        Health-Center
 * @copyright      2013 webriti
 * @license        license.txt
 * @author       :	webriti
 * @filesource     wp-content/themes/health=center/theme_setup_data.php
 *	Admin  & front end defual data file 
 *-----------------------------------------------------------------------------------*/ 
function theme_data_setup()
{	$tt=array();
	$tt[] = array('faq-title' => '','faq-text' => '');
	$tt[] = array('faq-title' => '','faq-text' => '');
	$tt[] = array('faq-title' => '','faq-text' => '');
	return $health_center_theme_options=array(
			// home project 
			 'project_list'=>4,
			  
			//Logo and Fevicon header			
			'hc_stylesheet'=>'default.css',
			'hc_back_image' =>'none.png',
			'upload_image_logo'=>'',
			'layout_selector' => 'boxed',
			'link_color_enable' => false,
			'link_color' => '#31A3DD',
			'height'=>'50',
			'width'=>'150',
			'hc_texttitle'=>'on',
			'custom_background_enabled'=>'off',
			'upload_image_favicon'=>'',
			'portfolio_title' =>'',
			'portfolio_description' =>'',
			'google_analytics'=>'',
			'webrit_custom_css'=>'',
			'call_out_text'=>'',
			'call_out_button_text'=>'',
			'call_out_button_link'=>'#',
			'call_out_button_link_target' =>true,			
			// front page
			'front_page_data'=>'Service,Project,News,Testimonials,Additional,CallOut',
			
			//Slide 	
			'home_slider_enabled'=>'on',
			'animation' => 'slide',								
			'animationSpeed' => 1500,
			'slide_direction' => 'horizontal',
			'slideshowSpeed' => 2000,
			
			//Page and Sections Headings
			'hc_head_news' =>'',
			'hc_head_faq' => '',
			'hc_head_testimonial' =>'',
			'hc_head_one_team' => '',
			'hc_head_two_team' => __('Great Team','health'),
			'hc_head_team_tagline' => __('We provide best WordPress solutions for your business. Thanks to our framework you will get more happy customers.','health'),
			
			//Post Type slug Options
			'hc_slider_slug' => '',
			'hc_service_slug' => '',
			'hc_portfolio_slug' => 'healthcenter_project',
			'hc_testimonial_slug' => 'healthcenter_testimonial',
			'hc_team_slug' => 'healthcenter_team',	
			'hc_products_category_slug' => 'portfolio_categories',			
			
			//Social media links
			'social_media_in_contact_page_enabled'=>'on',
			'header_social_media_enabled'=>'',
			'footer_social_media_enabled'=>'',
			'social_media_twitter_link' =>"https://twitter.com/",
			'social_media_facebook_link' =>"https:www.facebook.com",
			'social_media_linkedin_link' =>"http://linkedin.com/",
			'social_media_google_plus' =>"https://plus.google.com/",
			
			//contact page settings	
			'hc_get_in_touch_enabled'=>'on',
			'hc_get_in_touch' =>__('Get in Touch','health'),
			'hc_get_in_touch_description'=> 'Lorem ipsum dolor sit amet, usu rebum errem pericula ea, ei alia quaerendum vix. Ea justo tritani sit, odio ignota quo te. Lorem ipsum dolor sit amet.',
			
			'hc_send_usmessage' => __('Send Us a Message','health'),
			'hc_our_office_enabled'=>'on',
			'hc_contact_address'=> '25, Lorem Lis Street',
			'hc_contact_address_two'=> 'Dhanmandi California, US',
			'hc_contact_phone_number'=>'',
			'hc_contact_fax_number'=> '800 123 3456, US',
			'hc_contact_email'=>'',			
			'hc_contact_website'=>'https://www.webriti.com',
			
			'contact_google_map_enabled'=>'on',
			'hc_contact_google_map_url' => 'https://maps.google.co.in/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Kota+Industrial+Area,+Kota,+Rajasthan&amp;aq=2&amp;oq=kota+&amp;sll=25.003049,76.117499&amp;sspn=0.020225,0.042014&amp;t=h&amp;ie=UTF8&amp;hq=&amp;hnear=Kota+Industrial+Area,+Kota,+Rajasthan&amp;z=13&amp;ll=25.142832,75.879538',
			'footer_customizations' => __('@ 2016 Health Center. All Rights Reserved. Powered by','health'),
			'created_by_text' => __('Created by','health'),
			'created_by_webriti_text' => __('Webriti','health'),
			'created_by_link' => 'http://www.webriti.com',
			'powered_by_text' => __('WordPress','health'),
			
			'enable_custom_typography'=>false,
			
			// general typography			
			'general_typography_fontsize'=>14,
			'general_typography_fontfamily'=>'Open Sans',
			'general_typography_fontstyle'=>"normal",
			
			// menu title
			'menu_title_fontsize'=>14,
			'menu_title_fontfamily'=>'Raleway',
			'menu_title_fontstyle'=>"normal",
			
			//Section Title Typography
			'section_title_fontsize' => 36,
			'section_title_fontfamily' => 'Roboto',
			'section_title_fontstyle' => 'normal',
			
			//Section Sub Title Typography
			'section_subtitle_fontsize' => 18,
			'section_subtitle_fontfamily'=>'Roboto',
			'section_subtitle_fontstyle' => 'normal',
			
			
			// post title
			'post_title_fontsize'=>27,
			'post_title_fontfamily'=>'Open Sans',
			'post_title_fontstyle'=> "normal",
			
			// Service  title
			 'service_list' => 4,
			'service_title_fontsize'=>26,
			'service_title_fontfamily'=>'Open Sans',
			'service_title_fontstyle'=>"normal",
			
			// Potfolio  title Widget Heading Title
			'portfolio_title_fontsize'=>14,
			'portfolio_title_fontfamily'=>'Open Sans',
			'portfolio_title_fontstyle'=>"normal",
			
			// Widget Heading Title
			'widget_title_fontsize'=>32,
			'widget_title_fontfamily'=>'Open Sans',
			'widget_title_fontstyle'=>"normal",
			
			// Call out area Title   
			'calloutarea_title_fontsize'=>27,
			'calloutarea_title_fontfamily'=>'Raleway',
			'calloutarea_title_fontstyle'=>"normal",
			
			// Call out area descritpion      
			'calloutarea_description_fontsize'=>27,
			'calloutarea_description_fontfamily'=>'Raleway',
			'calloutarea_description_fontstyle'=>"normal",
			
			// Call out area purches button      
			'calloutarea_purches_fontsize'=>18,
			'calloutarea_purches_fontfamily'=>'Raleway',
			'calloutarea_purches_fontstyle'=>"normal",
			'hc_faq'=>$tt,
			
			// Taxonomy Archive Portfolio
			'taxonomy_portfolio_list' => 2,
			
			
			//New Data
			'slider_category' => '',
			
			'service_enable' => false,
			'service_title'=> '',
			'service_description' => '',
			'service_column_layout'=> 4,
			
			'project_enable' => false,
			'portfolio_title' =>'',
			'portfolio_description' =>'',
			'project_column_layout'=> 4,
			
			'news_enable' => false,
			'faq_enable' => false,
			
			'testimonial_enable' => false,
			
			'additional_enable' => false,
			'additional_column_layout'=>4,
			'additional_title' => '',
			'additional_description' => '',
			
			'callout_enable' => true,
			'site_intro_tex'=>'',
			'call_now_text' =>'',
			'call_now_number' =>'',
			
			'footer_copyright' => '',
			'hc_contact_google_map_shortcode'=>'',
			
			'home_slider_post_enable' => true,
			'home_testimonial_post_enable' => true,
		);
}
?>